import SketchMediator from './src/core/SketchMediator.js';

/* Create new sketch in instance mode
 * The Instance Mode has been used to:
 * - avoid polluting the global "namespace",
 *   thus allowing names similar to p5 methods to be used without conflicts
 * - enable ES6 module imports and exports,
 *   thus reducing the overhead of maintaining file load order in the html file  
 * - keep the number and size of global variables low
 */
new p5(SketchMediator, 'content');
