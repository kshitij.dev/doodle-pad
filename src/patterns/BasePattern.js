/* eslint-disable class-methods-use-this */
import ToolOptionsManager from '../managers/ToolOptionsManager.js';

/**
 * Base class for creating patterns with common methods and properties
 */
export default class BasePattern {
  /**
   * The p5 sketch instance associated with this pattern
   * @type {p5}
   */
  p5;

  /**
   * The graphics buffer used for masking
   * @type {p5.Graphics}
   */
  mask;

  /**
   * The graphics buffer used for preview
   * @type {p5.Graphics}
   */
  previewBuff;

  /**
   * The DOM element for pattern options
   * @type {p5.Element}
   */
  optionsDiv;

  /**
   * The tool options manager instance
   * @type {ToolOptionsManager}
   */
  options;

  /**
   * The name of the pattern
   * @type {string}
   */
  #name;

  /**
   * Creates a new BasePattern instance
   * @param {p5} p5 - The p5 instance
   * @param {p5.Graphics} mask - The graphics buffer for masking
   * @param {p5.Graphics} previewBuff - The graphics buffer for preview
   * @param {p5.Element} optionsDiv - The DOM element for pattern options
   * @param {String} name - The name of the pattern
   */
  constructor(p5, mask, previewBuff, optionsDiv, name) {
    this.mask = mask;
    this.previewBuff = previewBuff;
    this.p5 = p5;
    this.optionsDiv = optionsDiv;
    this.options = new ToolOptionsManager(this.p5);
    this.#name = name;
    this.populateOptions();
    this.hideOptions();
  }

  /**
   * Hides the pattern options
   */
  hideOptions() {
    this.options.hideAll();
  }

  /**
   * Shows the pattern options
   */
  showOptions() {
    this.options.showAll();
  }

  /**
   * Gets the name of the pattern
   * @returns {String} - The name of the pattern
   */
  getName() {
    return this.#name;
  }

  /**
   * Populates the pattern options
   * This method can be overridden in subclasses to add specific options
   */
  populateOptions() {
    // nothing to add as currently no common options across patterns
  }

  /**
   * Placeholder pattern generator
   */
  generate() { }
}
