import BasePattern from './BasePattern.js';

/**
 * Creates a coloured fabric pattern
 */
export default class FabricPattern extends BasePattern {
  /**
   * Creates a new ColouredFabricPattern instance
   * @param {p5} p5 - The p5 instance
   * @param {p5.Graphics} mainBuffer - The main graphics buffer
   * @param {p5.Graphics} demoBuffer - The demo graphics buffer
   * @param {p5.Element} optionsDiv - The options div element
   */
  constructor(p5, mainBuffer, demoBuffer, optionsDiv) {
    super(p5, mainBuffer, demoBuffer, optionsDiv, 'Fabric');
  }

  /**
   * Generates the coloured fabric pattern based on options
   */
  generate() {
    const [squareSize, lineWeight, lineColour, colour1, ...colours] = this.options.getList();
    const gridSize = squareSize.value();
    const lineWeightValue = lineWeight.value();
    const lineColourValue = lineColour.value();
    const colourCount = colours.length;

    this.mask.background(colour1.value());
    this.mask.strokeWeight(lineWeightValue);
    this.mask.textSize(gridSize);

    // Generate the pattern
    for (let x = 0; x < this.mask.width + gridSize; x += gridSize * 0.7) {
      for (let y = 0; y < this.mask.height + gridSize; y += gridSize * 1.2) {
        const randomValue = this.p5.random(3);
        this.mask.stroke(lineColourValue);
        this.mask.fill(colours[this.p5.floor(this.p5.random(colourCount))].value());
        if (randomValue < 1) {
          this.mask.text('░', x, y);
        } else if (randomValue < 2) {
          this.mask.text('▒', x, y);
        } else if (randomValue < 3) {
          this.mask.text('▓', x, y);
        } else {
          this.mask.text('█', x, y);
        }
      }
    }

    this.previewBuff.image(this.mask, 0, 0);
  }

  /**
   * Populates the options for the coloured fabric pattern
   */
  populateOptions() {
    this.options.addSlider('Size', 10, 100, 40, 10);
    this.options.addSlider('Line Weight', 1, 5, 1, 1);
    this.options.addColourPicker('Line Colour', 'black');
    this.options.addColourPicker('Colour 1', 'black');
    this.options.addColourPicker('Colour 2', 'red');
    this.options.addColourPicker('Colour 3', 'yellow');
    this.options.addColourPicker('Colour 4', 'green');
    this.options.addColourPicker('Colour 5', 'orange');
    this.options.addColourPicker('Colour 6', 'purple');

    this.options.changedAll(() => {
      this.generate();
    });
  }
}
