import BasePattern from './BasePattern.js';

/**
 * Creates a 10 print pattern
 */
export default class TenPrintPattern extends BasePattern {
  /**
   * Creates an instance of TenPrintPattern
   * @param {p5} p5 - The p5.js instance
   * @param {p5.Graphics} mainBuffer - The main graphics buffer for drawing
   * @param {p5.Graphics} demoBuffer - The demo graphics buffer for display
   * @param {p5.Element} optionsDiv - The options div element
   */
  constructor(p5, mainBuffer, demoBuffer, optionsDiv) {
    super(p5, mainBuffer, demoBuffer, optionsDiv, 'Ten Print');
  }

  /**
   * Generates the 10 print pattern based on the selected options
   */
  generate() {
    const [sizeSlider, lineWeight, lineColour, fillColour] = this.options.getList();

    // Set the background fill colour of the spaces
    this.mask.background(fillColour.value());

    // Set the colour of the squares and lines
    this.mask.fill(lineColour.value());
    this.mask.stroke(lineColour.value());
    this.mask.strokeWeight(lineWeight.value());

    const size = sizeSlider.value();

    // Generate the pattern
    for (let x = 0; x < this.mask.width; x += size) {
      for (let y = 0; y < this.mask.height; y += size) {
        const c = this.p5.random(2);
        if (c < 1) {
          this.mask.line(x, y, x + size, y + size);
        } else if (c < 2) {
          this.mask.line(x, y + size, x + size, y);
        } else if (c < 3) {
          this.mask.line(x, y, x, y + size);
        } else if (c < 4) {
          this.mask.line(x, y, x + size, y);
        }
      }
    }

    // Display the generated pattern
    this.previewBuff.image(this.mask, 0, 0);
  }

  /**
   * Populates pattern options with appropriate sliders and pickers
   */
  populateOptions() {
    this.options.addSlider('Size', 10, 100, 25, 5);
    this.options.addSlider('Line Weight', 1, 10, 5, 1);
    this.options.addColourPicker('Line Colour', 'black');
    this.options.addColourPicker('Fill Colour', 'orange');

    // Regenerate pattern when options change
    this.options.changedAll(() => { this.generate(); });
  }
}
