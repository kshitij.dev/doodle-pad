import BasePattern from './BasePattern.js';

/**
 * Creates a petals pattern:
 */
export default class FlowerPetalsPattern extends BasePattern {
  /**
   * Create a new FlowerPetalsPattern instance
   * @param {p5} p5 - The p5 instance
   * @param {p5.Graphics} mainBuffer - The main graphics buffer
   * @param {p5.Graphics} demoBuffer - The demo graphics buffer
   * @param {p5.Element} optionsDiv - The options div element
   */
  constructor(p5, mainBuffer, demoBuffer, optionsDiv) {
    super(p5, mainBuffer, demoBuffer, optionsDiv, 'Flower Petals');
  }

  /**
   * Generate the flower petals pattern based on options
   */
  generate() {
    const [
      spacing,
      internalSpace,
      petalSize,
      lineWeight, lineColour, fillColour
    ] = this.options.getList();

    // Fill colour of the spaces
    this.mask.background(fillColour.value());

    // Colour of the petals and lines
    this.mask.fill(lineColour.value());
    this.mask.stroke(lineColour.value());
    this.mask.strokeWeight(lineWeight.value());
    const space = spacing.value();
    const intSpace = internalSpace.value();
    const size = petalSize.value();

    const drawCurves = (x, y) => {
      for (let i = 0; i < 8; i++) {
        this.mask.push();
        this.mask.translate(x, y);
        this.mask.rotate(i * this.p5.PI * 0.25);
        this.mask.beginShape();
        this.mask.curveVertex(intSpace + size, 0);
        this.mask.curveVertex(intSpace + size, 0);
        this.mask.curveVertex(intSpace + size / 4, -size / 4);
        this.mask.curveVertex(intSpace, 0);
        this.mask.curveVertex(intSpace + size / 4, +size / 4);
        this.mask.curveVertex(intSpace + size, 0);
        this.mask.curveVertex(intSpace + size, 0);
        this.mask.endShape();
        this.mask.pop();
      }
    };

    // Generate the pattern
    for (let x = -50; x < this.mask.width + 50; x += space) {
      for (let y = -50; y < this.mask.height + 50; y += space) {
        drawCurves(x, y);
      }
    }

    for (let x = -50 + space / 2; x < this.mask.width + 50; x += space) {
      for (let y = -50 + space / 2; y < this.mask.height + 50; y += space) {
        drawCurves(x, y);
      }
    }

    this.previewBuff.image(this.mask, 0, 0);
  }

  /**
   * Populate the options for the flower petals pattern
   */
  populateOptions() {
    this.options.addSlider('Spacing', 10, 100, 70, 5);
    this.options.addSlider('Internal Space', 10, 100, 60, 5);
    this.options.addSlider('Size', 5, 20, 10, 5);
    this.options.addSlider('Line Weight', 1, 10, 2, 1);
    this.options.addColourPicker('Line Colour', 'teal');
    this.options.addColourPicker('Fill Colour', 'maroon');

    this.options.changedAll(() => { this.generate(); });
  }
}
