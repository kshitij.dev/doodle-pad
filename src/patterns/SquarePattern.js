import BasePattern from './BasePattern.js';

/**
 * Creates a square pattern
 */
export default class SquarePattern extends BasePattern {
  /**
   * Creates an instance of SquarePattern
   * @param {p5} p5 - The p5 instance
   * @param {p5.Graphics} mainBuffer - The main graphics buffer
   * @param {p5.Graphics} demoBuffer - The demo graphics buffer
   * @param {p5.Element} optionsDiv - The options div element
   */
  constructor(p5, mainBuffer, demoBuffer, optionsDiv) {
    super(p5, mainBuffer, demoBuffer, optionsDiv, 'Simple Squares');
  }

  /**
   * Generates the square pattern based on the selected options
   */
  generate() {
    const [
      spacing, square1Size,
      square2Size, lineWeight,
      lineColour, fillColour
    ] = this.options.getList();

    // Set the background fill colour of the spaces
    this.mask.background(fillColour.value());

    // Set the colour of the squares and lines
    this.mask.fill(lineColour.value());
    this.mask.stroke(lineColour.value());
    this.mask.strokeWeight(lineWeight.value());
    this.mask.rectMode(this.mask.CENTER);

    // Generate the pattern
    const s = spacing.value();
    for (let x = 0; x < this.mask.width + s; x += s) {
      for (let y = 0; y < this.mask.height + s; y += s) {
        this.mask.line(x, y, x + s, y);
        this.mask.line(x, y, x, y + s);
        this.mask.square(x, y, square1Size.value());
        this.mask.square(x + s / 2, y + s / 2, square2Size.value());
      }
    }

    // Display the generated pattern
    this.previewBuff.image(this.mask, 0, 0);
  }

  /**
   * Populates pattern options with appropriate sliders and pickers
   */
  populateOptions() {
    this.options.addSlider('Spacing', 20, 100, 50, 5);
    this.options.addSlider('Square 1', 1, 75, 10, 5);
    this.options.addSlider('Square 2', 1, 75, 10, 5);
    this.options.addSlider('Line Weight', 1, 10, 3, 1);
    this.options.addColourPicker('Line Colour', 'blue');
    this.options.addColourPicker('Fill Colour', 'yellow');

    // Regenerate pattern when options change
    this.options.changedAll(() => { this.generate(); });
  }
}
