import BasePattern from './BasePattern.js';

/**
 * Creates a Mondrian pattern
 */
export default class MondrianPattern extends BasePattern {
  /**
   * Creates an instance of MondrianPattern
   * @param {p5} p5 - The p5 instance
   * @param {p5.Graphics} mainBuffer - The main graphics buffer
   * @param {p5.Graphics} demoBuffer - The demo graphics buffer
   * @param {p5.Element} optionsDiv - The options div element
   */
  constructor(p5, mainBuffer, demoBuffer, optionsDiv) {
    super(p5, mainBuffer, demoBuffer, optionsDiv, 'Mondrian');
  }

  /**
   * Generates the Mondrian pattern based on the selected options
   */
  generate() {
    const [sizeSlider, lineWeight, width, height, lineColour, ...colours] = this.options.getList();

    // Set the colour of the squares and lines
    this.mask.fill(lineColour.value());
    this.mask.stroke(lineColour.value());
    this.mask.strokeWeight(lineWeight.value());

    const size = sizeSlider.value();
    const widthMult = width.value();
    const heightMult = height.value();
    const col = [];
    colours.forEach((colour) => col.push(colour.value()));

    // Generate the pattern
    for (let x = this.mask.width; x > -size * widthMult; x -= size) {
      for (let y = this.mask.height; y > -size * heightMult; y -= size) {
        this.mask.fill(col[this.p5.floor(this.p5.random(3))]);
        this.mask.push();
        this.mask.translate(x + size / 2, y + size / 2);

        this.mask.rect(
          0,
          0,
          size * this.p5.floor(this.p5.random(1, widthMult)),
          size * this.p5.floor(this.p5.random(1, heightMult))
        );
        this.mask.pop();
      }
    }

    // Display the generated pattern
    this.previewBuff.image(this.mask, 0, 0);
  }

  /**
   * Populates pattern options with appropriate sliders and pickers
   */
  populateOptions() {
    this.options.addSlider('Size', 10, 100, 30, 5);
    this.options.addSlider('Line Weight', 0, 5, 0, 1);
    this.options.addSlider('Width', 1, 20, 4, 1);
    this.options.addSlider('Height', 1, 20, 4, 1);
    this.options.addColourPicker('Line Colour', 'black');
    this.options.addColourPicker('Colour 1', 'yellow');
    this.options.addColourPicker('Colour 2', 'teal');
    this.options.addColourPicker('Colour 3', 'fuchsia');

    // Regenerate pattern when options change
    this.options.changedAll(() => { this.generate(); });
  }
}
