import BasePattern from './BasePattern.js';

/**
 * Creates a diamond pattern
 */
export default class DiamondPattern extends BasePattern {
  /**
   * Create a new DiamondPattern instance
   * @param {p5} p5 - The p5 instance
   * @param {p5.Graphics} mainBuffer - The main graphics buffer
   * @param {p5.Graphics} demoBuffer - The demo graphics buffer
   * @param {p5.Element} optionsDiv - The options div element
   */
  constructor(p5, mainBuffer, demoBuffer, optionsDiv) {
    super(p5, mainBuffer, demoBuffer, optionsDiv, 'Diamonds');
  }

  /**
   * Generate the diamond pattern based on options
   */
  generate() {
    const [spacing, lineWeight, lineColour, fillColour] = this.options.getList();

    // Fill colour of the spaces
    this.mask.background(fillColour.value());

    // Colour of the diamonds and lines
    this.mask.fill(lineColour.value());
    this.mask.stroke(lineColour.value());
    this.mask.strokeWeight(lineWeight.value());
    const space = spacing.value();

    const drawTriangles = (x, y) => {
      this.mask.triangle(x, y - 30, x - 3, y - 10, x + 3, y - 10);
      this.mask.triangle(x, y + 30, x - 3, y + 10, x + 3, y + 10);
      this.mask.triangle(x - 30, y, x - 10, y - 3, x - 10, y + 3);
      this.mask.triangle(x + 30, y, x + 10, y - 3, x + 10, y + 3);
    };

    for (let x = 0; x < this.mask.width + 50; x += space) {
      for (let y = 0; y < this.mask.height + 50; y += space) {
        drawTriangles(x, y);
      }
    }

    for (let x = space / 2; x < this.mask.width + 50; x += space) {
      for (let y = space / 2; y < this.mask.height + 50; y += space) {
        drawTriangles(x, y);
      }
    }

    this.previewBuff.image(this.mask, 0, 0);
  }

  /**
   * Populate the options for the diamond pattern
   */
  populateOptions() {
    this.options.addSlider('Spacing', 30, 100, 60, 5);
    this.options.addSlider('Line Weight', 1, 10, 2, 1);
    this.options.addColourPicker('Line Colour', 'red');
    this.options.addColourPicker('Fill Colour', 'black');

    this.options.changedAll(() => { this.generate(); });
  }
}
