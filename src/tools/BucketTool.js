import BaseTool from './BaseTool.js';

/**
 * Represents the Bucket Tool class, inheriting from BaseTool
 */
export default class BucketTool extends BaseTool {
  // The type of cursor used by the tool
  cursor;

  // Enumeration for cursor types
  CURSOR_TYPE;

  // Reference to the content CSS element
  #contentCSS;

  /**
   * Creates an instance of the Bucket Tool
   */
  constructor(instance) {
    super(instance, 'Bucket', 'Bucket fill', 'assets/bucket.svg');

    // Define cursor types
    this.CURSOR_TYPE = {
      PLUS: 0,
      WAIT: 1
    };

    // Set initial cursor type
    this.cursor = this.CURSOR_TYPE.PLUS;

    // Populate tool options and hide them
    this.populateOptions();
    this.toolOptions.hideAll();

    // Store a reference to the content CSS element
    this.#contentCSS = this.p5.select('.content');
  }

  /**
   * Called when the mouse button is pressed
   */
  mousePressed() {
    this.cursor = this.CURSOR_TYPE.WAIT;
  }

  /**
   * Called when the mouse button is released
   */
  mouseReleased() {
    if (this.inputDevice.isInCanvas()) {
      this.inputDevice.updateCurrentPosition();
      let { x, y } = this.inputDevice.getCurrentPosition();

      // Use integers to avoid unpredictable filling at borders
      x = parseInt(x.toString(), 10);
      y = parseInt(y.toString(), 10);

      const [opacity, colour] = this.toolOptions.getList();
      const newColour = this.p5.color(colour.value());
      newColour.setAlpha(opacity.value());
      const isNewFill = this.floodFill(this.p5.createVector(x, y), newColour.levels);
      if (isNewFill) this.instance.historyManager.addToUndoStack();
    }
    this.cursor = this.CURSOR_TYPE.PLUS;
  }

  /**
   * Indirectly called by the p5 draw function
   */
  draw() {
    if (this.cursor === this.CURSOR_TYPE.PLUS) {
      this.#contentCSS.style('cursor', 'crosshair');
    } else {
      this.#contentCSS.style('cursor', 'wait');
    }
  }

  /**
   * Populates tool options
   */
  populateOptions() {
    super.populateOptions();
    this.toolOptions.addSlider('Opacity', 0, 255, 255, 5);
    this.toolOptions.addColourPicker('Fill Colour', 'red');
  }

  /**
   * Called when the tool is deselected
   */
  unselectTool() {
    this.#contentCSS.style('cursor', 'crosshair');
    super.unselectTool();
  }

  /**
   * Checks if two colours are the same
   */
  isSameColour(colour1, colour2) {
    return colour1.every((level, index) => level === colour2[index]);
  }

  /**
   * Adds neighbouring pixels to the queue for processing
   * @param {Array} queue - The queue of pixels to be processed
   * @param {p5.Vector} pixel - The current pixel being processed
   * @returns {Array} - The updated queue of pixels
   */
  addNeighbours(queue, pixel) {
    const { x, y } = pixel;

    // Check if there's a pixel to the left
    if (x - 1 > 0) {
      queue.push(this.p5.createVector(x - 1, y));
    }

    // Check if there's a pixel to the right
    if (x + 1 < this.canvas.size().width) {
      queue.push(this.p5.createVector(x + 1, y));
    }

    // Check if there's a pixel above
    if (y - 1 > 0) {
      queue.push(this.p5.createVector(x, y - 1));
    }

    // Check if there's a pixel below
    if (y + 1 < this.canvas.size().height) {
      queue.push(this.p5.createVector(x, y + 1));
    }

    // Return the updated queue of pixels
    return queue;
  }

  /**
   * Flood fills the pixels starting from the seed position
   * @param {p5.Vector} seed - The seed position to start filling from
   * @param {Array} fillColour - The RGBA colour to fill with
   * @returns {Boolean} - Indicates whether a new fill occurred
   */
  floodFill(seed, fillColour) {
    // Load the pixel data of the canvas
    this.p5.loadPixels();

    // Get the properties of the seed pixel
    const seedProperties = this.getProperties(seed);

    // Check if the fill colour is the same as the seed colour
    if (this.isSameColour(fillColour, seedProperties.colour)) {
      // If the fill colour is the same, no new fill occurred
      return false;
    }

    // Initialise a queue for the flood fill algorithm
    let queue = [];
    queue.push(seed);

    while (queue.length) {
      // Take the next pixel from the queue
      const current = queue.shift();

      // Get properties of the current pixel
      const currentProperties = this.getProperties(current);

      // Check if the current pixel's colour matches the seed colour
      if (this.isSameColour(currentProperties.colour, seedProperties.colour)) {
        // Update the pixel's colour with the new fill colour
        for (let i = 0; i < 4; i++) {
          this.p5.pixels[currentProperties.index + i] = fillColour[i];
        }

        // Add neighbouring pixels to the queue for further processing
        queue = this.addNeighbours(queue, current);
      }
    }

    // Update the canvas with the modified pixel data
    this.p5.updatePixels();

    // Indicate that a new fill occurred
    return true;
  }

  /**
   * Retrieves properties of a pixel based on its position
   * @param {p5.Vector} pix - The position of the pixel
   * @returns {Object} - An object containing pixel properties
   */
  getProperties(pix) {
    // Calculate the index of the pixel in the pixel array
    const index = 4 * (this.canvas.size().width * pix.y + pix.x);

    // Extract the RGBA colour values of the pixel
    const colour = [
      this.p5.pixels[index],
      this.p5.pixels[index + 1],
      this.p5.pixels[index + 2],
      this.p5.pixels[index + 3]
    ];

    // Return an object containing the colour values and index
    return { colour, index };
  }
}
