import BaseTool from './BaseTool.js';
import ShapeFactory from '../core/ShapeFactory.js';

/**
 * A tool that enables the user to draw various geometrical shapes
 * @extends {BaseTool}
 */
export default class GeometricalShapesTool extends BaseTool {
  /**
   * The currently selected geometrical shape
   * @type {String}
   */
  #selectedShape;

  /**
   * Indicates whether the shape should be filled
   * @type {Boolean}
   */
  #isFilled;

  /**
   * The previously selected number of sides for polygons
   * @type {Number}
   */
  #prevSides;

  /**
   * The previously selected angle for polygons
   * @type {Number}
   */
  #prevAngle;

  /**
   * Create an instance of GeometricalShapesTool
   */
  constructor(
    instance,
    name = 'GeometricalShapes',
    toolTip = 'Geometrical shapes',
    icon = 'assets/shapes.svg'
  ) {
    super(instance, name, toolTip, icon);

    // Initialise private fields
    this.#selectedShape = 'ScribbleRectangle';
    this.#isFilled = false;
    this.#prevSides = 5;
    this.#prevAngle = 45;

    // Populate tool options and hide them by default
    this.populateOptions();
    this.toolOptions.hideAll();
  }

  /**
   * Draws the chosen geometrical shape if the mouse is pressed
   */
  drawMousePressed() {
    // If it's the first frame, store the starting mouse position
    if (this.inputDevice.isFirstFrame()) {
      // Change to drawing state
      this.isDrawing = true;

      // Record the starting mouse position
      this.inputDevice.updateInitialPosition();

      // Save the current state of the canvas (without pushing to undo stack)
      this.p5.loadPixels();
    } else {
      // We are already drawing, so load the last saved pixels
      this.p5.updatePixels();

      // Draw the selected geometrical shape
      const [, colour, sides, angle] = this.toolOptions.getList();

      const config = {
        fillColour: colour.value(),
        strokeWeight: this.instance.strokeWeightManager.getWeight(),
        polySides: sides.value(),
        polyAngle: angle.value(),
        type: this.#selectedShape,
        isFilled: this.#isFilled,
        stroke: this.instance.colourPalette.getSelectedColour()
      };

      this.inputDevice.updateCurrentPosition();

      // Create and draw the shape using ShapeFactory
      (new ShapeFactory(this.p5, config)).drawShape(
        this.inputDevice.getInitialPosition(),
        this.inputDevice.getCurrentPosition()
      );

      // Note: The pixels will be saved to the undo stack by the base class method drawMouseFree
    }
  }

  /**
   * Populates tool options with various configuration settings
   * Overrides the base method to add geometrical shape-specific options
   */
  populateOptions() {
    super.populateOptions();

    const shapes = [
      'ScribbleRectangle',
      'ScribbleEllipse',
      'Ellipse',
      'Rectangle',
      'Triangle',
      'Polygon'
    ];

    this.toolOptions.addSelect('Shape', shapes);
    this.toolOptions.addColourPicker('Fill Colour', 'red');
    this.toolOptions.addInput('Sides', '5');
    this.toolOptions.addInput('Angle', '45');
    this.toolOptions.addCheckbox('With Fill', this.#isFilled);
    this.addToolOptionCallbacks();
    this.togglePolygonOptions();
  }

  /**
   * Adds callback functions for various tool options
   * Handles validation and updates for sides, angle, and fill settings
   */
  addToolOptionCallbacks() {
    const [shape, , sides, angle, checkbox] = this.toolOptions.getList();

    sides.changed(() => {
      const newValue = sides.value();
      if (newValue.match(/\D/)) {
        alert(`"${sides.value()}" is an invalid polygon size.`);
        sides.value(this.#prevSides);
      } else {
        this.#prevSides = newValue;
      }
    });

    angle.changed(() => {
      const newValue = angle.value();
      if (newValue.match(/\D/)) {
        alert(`"${angle.value()}" is an invalid polygon angle.`);
        angle.value(this.#prevAngle);
      } else {
        this.#prevAngle = newValue;
      }
    });

    checkbox.changed(() => {
      this.#isFilled = !this.#isFilled;
    });

    shape.selected(this.#selectedShape);

    shape.changed(() => {
      this.#selectedShape = shape.value();
      this.togglePolygonOptions();
    });
  }

  /**
   * Toggles the visibility of options related to polygon shapes
   * Hides or shows the sides and angle options based on the selected shape
   */
  togglePolygonOptions() {
    const [shape, , sides, angle] = this.toolOptions.getList();
    switch (shape.value()) {
      case 'Polygon':
        sides.show();
        angle.show();
        break;

      default:
        sides.hide();
        angle.hide();
        break;
    }
  }

  /**
   * Shows tool options along with the colour palette and relevant shape options
   * Overrides the base method to include shape-specific options display
   */
  showOptions() {
    super.showOptions();
    this.instance.colourPalette.show();
    this.togglePolygonOptions();
  }
}
