import BaseTool from './BaseTool.js';
import ShapeFactory from '../core/ShapeFactory.js';

/**
 * A tool that enables the user to erase content
 * @extends {BaseTool}
 */
export default class EraserTool extends BaseTool {
  /**
   * Create an instance of EraserTool
   */
  constructor(instance) {
    super(instance, 'Eraser', 'Erasers of various sizes', 'assets/eraser.svg');

    // Populate the tool options and hide them by default
    this.populateOptions();
    this.toolOptions.hideAll();
  }

  /**
   * Draws a Rectangle with the size chosen by the user to simulate erasing
   * Adjusts the position and size of the Rectangle based on mouse position and tool options
   */
  drawMousePressed() {
    this.isDrawing = true;
    const [size] = this.toolOptions.getList();
    this.inputDevice.updateCurrentPosition();

    // Determine the coordinates of the rectangle for the shape factory,
    // which expects the top-left and bottom-right coordinates
    const from = this.inputDevice.getCurrentPosition().copy();
    const to = from.copy();

    // Center the rectangle at the crosshair position
    to.add(size.value() / 2, size.value() / 2);
    from.sub(size.value() / 2, size.value() / 2);

    // Create and draw a filled rectangle using ShapeFactory to simulate erasing
    new ShapeFactory(this.p5, {
      type: 'Rectangle',
      strokeWeight: 0,
      isFilled: true,
      fillColour: 'white'
    }).drawShape(from, to);
  }

  /**
   * Populates the tool options with the eraser size slider
   */
  populateOptions() {
    this.toolOptions.addSlider('Size', 2, 100, 25, 2);
  }
}
