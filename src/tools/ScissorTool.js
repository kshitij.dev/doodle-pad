import BaseTool from './BaseTool.js';
import ShapeFactory from '../core/ShapeFactory.js';

/**
 * A tool that enables the user to cut-and-paste parts of the canvas
 * @extends {BaseTool}
 */
export default class ScissorTool extends BaseTool {
  /**
   * Current selection mode
   * @type {Number}
   */
  #selectMode;

  /**
   * Object holding the selected area dimensions and coordinates
   * @type {{x: (Number), y: (Number), w: (Number), h: (Number)}}
   */
  #selectedArea;

  /**
   * The pixels in the cut area
   * @type {p5.Image}
   */
  #cutPixels;

  /**
   * Enumeration for the selection mode options
   * @type {{Cut: (Number), Paste: (Number)}}
   */
  #selectModeOptions;

  /**
   * Construct a new ScissorTool instance
   */
  constructor(instance) {
    super(instance, 'Scissors', 'Scissor - cut and paste', 'assets/scissors.svg');

    // Initialise properties
    this.#resetSelectedArea();
    this.#cutPixels = undefined;
    this.#selectModeOptions = { Cut: 1, Paste: 2 };
    this.#selectMode = this.#selectModeOptions.Cut;

    this.populateOptions();
    this.toolOptions.hideAll();
  }

  /**
   * Draw the guide rectangle if the mouse is pressed and in cut mode
   */
  drawMousePressed() {
    // Check if in cut mode
    if (this.#selectMode === this.#selectModeOptions.Cut) {
      // Load pixels from the pixel array
      this.p5.updatePixels();
      const { from, to } = this.#getSelectedCoordinates();

      (new ShapeFactory(this.p5, {
        strokeWeight: 2, isStrokeDashed: true, type: 'Rectangle', stroke: 'lightgray'
      })).drawShape(from, to);
    }
  }

  /**
   * Add tool-specific options buttons and their click handlers
   */
  populateOptions() {
    // Create the options button
    this.toolOptions.addButton('Paste Mode');
    const [button] = this.toolOptions.getList();

    // Add the click handler arrow function
    button.changed(() => {
      // Check if in cut mode
      if (this.#selectMode === this.#selectModeOptions.Cut) {
        // Change mode to paste
        this.#selectMode = this.#selectModeOptions.Paste;

        // Update label
        button.rename('Cut Mode');
      } else if (this.#selectMode === this.#selectModeOptions.Paste) {
        // Change to cut mode
        this.#selectMode = this.#selectModeOptions.Cut;

        // Save the current pixels into the pixel array
        this.p5.loadPixels();

        // Reset selected area properties to default
        this.#resetSelectedArea();

        // Update label to paste
        button.rename('Paste Mode');
      }
    });
  }

  /**
   * Update the rectangle dimensions when the mouse is dragged
   */
  mouseDragged() {
    this.inputDevice.updateCurrentPosition();
    const { x, y } = this.inputDevice.getCurrentPosition();
    if (this.#selectMode === this.#selectModeOptions.Cut && this.inputDevice.isInCanvas()) {
      this.#selectedArea.w = x - this.#selectedArea.x;
      this.#selectedArea.h = y - this.#selectedArea.y;
    }
  }

  /**
   * Paste the image centered at the cursor in paste mode and
   * update rectangle position in cut mode
   */
  mousePressed() {
    this.inputDevice.updateCurrentPosition();
    const { x, y } = this.inputDevice.getCurrentPosition();

    // Check if in cut mode and within canvas
    if (this.#selectMode === this.#selectModeOptions.Cut && this.inputDevice.isInCanvas()) {
      // Update position from which the rectangle will be drawn
      this.#selectedArea.x = x;
      this.#selectedArea.y = y;
    } else if (
      this.#selectMode === this.#selectModeOptions.Paste &&
      this.inputDevice.isInCanvas()
    ) {
      // Paste the center of the image at the mouse position
      this.p5.image(
        this.#cutPixels,
        x - this.#cutPixels.width / 2,
        y - this.#cutPixels.height / 2
      );

      // Once pasted, push the pixel array to the draw stack
      this.instance.historyManager.addToUndoStack();
    }
  }

  /**
   * Remove the aiding rectangle if in cut mode and
   * draw a white box over the area
   */
  mouseReleased() {
    // Check if in cut mode and within canvas
    if (this.#selectMode === this.#selectModeOptions.Cut && this.inputDevice.isInCanvas()) {
      // Get rid of the rectangle
      this.p5.updatePixels();

      // Store the selected pixels
      this.#cutPixels = this.p5.get(
        this.#selectedArea.x,
        this.#selectedArea.y,
        this.#selectedArea.w,
        this.#selectedArea.h
      );

      // Draw a rectangle over it
      const { from, to } = this.#getSelectedCoordinates();
      (new ShapeFactory(this.p5, {
        strokeWeight: 0, isFilled: true, type: 'Rectangle', fillColour: 'white',
      })).drawShape(from, to);

      // Store pixels in the pixel array
      this.p5.loadPixels();

      // Push pixels to the undo stack
      this.instance.historyManager.addToUndoStack();

      this.#resetSelectedArea();
    }
  }

  /**
   * Reset and clean up once the tool is deselected
   */
  unselectTool() {
    // Reset private variables back to default
    this.#resetSelectedArea();
    this.#cutPixels = undefined;
    this.#selectMode = this.#selectModeOptions.Cut;
    const [button] = this.toolOptions.getList();
    button.rename('Paste Mode');

    super.unselectTool();
  }

  /**
   * Reset the selected area
   */
  #resetSelectedArea() {
    this.#selectedArea = {
      x: 0, y: 0, w: 1, h: 1,
    };
  }

  /**
   * Get the selected coordinates as vectors
   * @returns {{from: p5.Vector, to: p5.Vector}} - The selected coordinates
   */
  #getSelectedCoordinates() {
    const from = this.p5.createVector(this.#selectedArea.x, this.#selectedArea.y);
    const to = this.p5.createVector(
      this.#selectedArea.x + this.#selectedArea.w,
      this.#selectedArea.y + this.#selectedArea.h
    );

    return { from, to };
  }
}
