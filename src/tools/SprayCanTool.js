import BaseTool from './BaseTool.js';
import ShapeFactory from '../core/ShapeFactory.js';

/**
 * A tool that enables the user to draw with a spray can
 * @extends {BaseTool}
 */
export default class SprayCanTool extends BaseTool {
  /**
   * Construct a new SprayCanTool instance
   */
  constructor(instance) {
    super(instance, 'Spray', 'Spray can', 'assets/sprayCan.svg');

    this.populateOptions();
    this.toolOptions.hideAll();
  }

  /**
   * Draw a series of points when the mouse is pressed
   */
  drawMousePressed() {
    this.inputDevice.updateCurrentPosition();
    const { x, y } = this.inputDevice.getCurrentPosition();
    // Toggle the drawing state
    this.isDrawing = true;

    const [densitySlider, spreadSlider] = this.toolOptions.getList();

    // Loop over all points and draw spray with some randomness
    for (let i = 0; i < densitySlider.value(); i++) {
      this.drawSpray(
        this.p5.random(x - spreadSlider.value(), x + spreadSlider.value()),
        this.p5.random(y - spreadSlider.value(), y + spreadSlider.value()),
      );
    }
  }

  /**
   * Draw a single point of the spray at the given coordinates
   * @param {Number} x - The horizontal coordinate of the point
   * @param {Number} y - The vertical coordinate of the point
   */
  drawSpray(x, y) {
    // Get the currently selected colour
    const [, , colourPicker] = this.toolOptions.getList();
    const colour = colourPicker.value();
    const position = this.p5.createVector(x, y);

    // Draw the point
    new ShapeFactory(this.p5, {
      type: 'Point',
      strokeWeight: Math.max(1, this.instance.strokeWeightManager.getWeight() * 0.15),
      stroke: colour
    }).drawShape(position, position);
  }

  /**
   * Populate the tool options with additional controls
   */
  populateOptions() {
    super.populateOptions();
    this.toolOptions.addSlider('Density', 10, 100, 35, 5);
    this.toolOptions.addSlider('Spread', 10, 30, 16, 2);
    this.toolOptions.addColourPicker('Spray Colour', 'red');
  }
}
