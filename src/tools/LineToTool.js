import BaseTool from './BaseTool.js';
import ShapeFactory from '../core/ShapeFactory.js';

/**
 * A tool for drawing straight lines to the screen Allows the user to preview
 * the line to the current mouse position before drawing the line to the
 * pixel array
 * @extends {BaseTool}
 */
export default class LineToTool extends BaseTool {
  /**
   * Create a new instance of the LineToTool
   */
  constructor(instance) {
    super(instance, 'Line', 'Straight-line pencil', 'assets/lineTo.svg');

    // Populate the tool options and hide them initially
    this.populateOptions();
    this.toolOptions.hideAll();
  }

  /**
   * Draws the line if mouse is pressed
   */
  drawMousePressed() {
    // Check if this is the start of drawing a new line
    if (this.inputDevice.isFirstFrame()) {
      // Store the initial mouse position
      this.inputDevice.updateInitialPosition();

      // Toggle the drawing state
      this.isDrawing = true;

      // Save the current pixel array state
      this.p5.loadPixels();
    } else {
      /**
       * update the screen with the saved pixels to hide
       * any previous line between mouse pressed and released
       */
      this.p5.updatePixels();

      // Update the current position of the input device and draw the line
      this.inputDevice.updateCurrentPosition();
      const config = {
        type: 'Line',
        strokeWeight: this.instance.strokeWeightManager.getWeight(),
        stroke: this.instance.colourPalette.getSelectedColour()
      };

      // Create a new line shape and draw it
      new ShapeFactory(this.p5, config)
        .drawShape(this.inputDevice.getInitialPosition(), this.inputDevice.getCurrentPosition());
    }
  }

  /**
   * Show the options specific to this tool, including the colour palette
   */
  showOptions() {
    super.showOptions();
    this.instance.colourPalette.show();
  }
}
