import BaseTool from './BaseTool.js';
import ShapeFactory from '../core/ShapeFactory.js';

/**
 * A tool that allows the user to draw symmetrically
 * either horizontally or vertically
 * @extends {BaseTool}
 */
export default class MirrorDrawTool extends BaseTool {
  /**
   * Object containing initial and current coordinates of mirrored side
   */
  oppositeCoordinates;

  /**
   * axis being mirrored (x or y)
   @type {String}
   */
  #axis;

  /**
   * line of symmetry position
   @type {Number}
   */
  #lineOfSymmetry;

  /**
   * Create a new instance of the MirrorDrawTool
   */
  constructor(instance) {
    super(instance, 'Mirror', 'Mirrored free-hand pencil', 'assets/mirrorDraw.svg');

    // initialise with vertical mirror
    this.#axis = 'y';

    // set halfway vertically as we have initialised as so
    this.#lineOfSymmetry = this.p5.height / 2;

    this.populateOptions();
    this.toolOptions.hideAll();

    // Initialise opposite coordinates
    this.oppositeCoordinates = {
      initial: this.p5.createVector(),
      current: this.p5.createVector()
    };
  }

  /**
   * Override the base method to draw the free hand
   * across the line of symmetry
   */
  drawMousePressed() {
    // Load pixel stored in pixel array
    this.p5.updatePixels();

    // Calculate opposite coordinates
    this.inputDevice.updateCurrentPosition();
    this.oppositeCoordinates.current = this.p5.createVector(
      this.#calculateOpposite(this.inputDevice.getCurrentPosition().x, 'x'),
      this.#calculateOpposite(this.inputDevice.getCurrentPosition().y, 'y')
    );

    /**
     * If first frame
     * initialise start mouse position and
     * mirrored position
     */
    if (this.inputDevice.isFirstFrame()) {
      // Toggle drawing state
      this.isDrawing = true;

      // Initialise mouse positions
      this.inputDevice.updateInitialPosition();
      this.oppositeCoordinates.initial = this.oppositeCoordinates.current;
    } else {
      // Draw the lines
      const config = {
        type: 'Line',
        strokeWeight: this.instance.strokeWeightManager.getWeight(),
        stroke: this.instance.colourPalette.getSelectedColour()
      };

      const line = new ShapeFactory(this.p5, config);
      line.drawShape(this.inputDevice.getInitialPosition(), this.inputDevice.getCurrentPosition());
      line.drawShape(this.oppositeCoordinates.initial, this.oppositeCoordinates.current);

      // Reset start mouse positions on both sides
      this.inputDevice.updateInitialPosition();
      this.oppositeCoordinates.initial = this.oppositeCoordinates.current;
    }

    // Save pixels for next loop
    this.p5.loadPixels();

    // Draw the line of symmetry
    this.#drawLineOfSymmetry();
  }

  /**
   * When mouse is not pressed and if previously drawing,
   * push the pixels to undo stack
   */
  drawMouseFree() {
    // Remove line of symmetry by reloading pixels
    this.p5.updatePixels();
    this.p5.loadPixels();

    // If previously in drawing state, push pixels to undo stack
    if (this.isDrawing) {
      // toggle drawing state
      this.isDrawing = false;

      // Reset mouse start position
      this.inputDevice.resetState();
      this.oppositeCoordinates.initial = this.p5.createVector();

      // Push pixels to undo stack
      this.instance.historyManager.addToUndoStack();
    }

    // Draw line of symmetry (not included in undo stack)
    this.#drawLineOfSymmetry();
  }

  /**
   * Draws the line of symmetry
   */
  #drawLineOfSymmetry() {
    // If x-axis, draw vertical line otherwise horizontal line
    const from = {
      x: this.#axis === 'x' ? this.canvas.size().width / 2 : 0,
      y: this.#axis === 'x' ? 0 : this.canvas.size().height / 2
    };

    const to = {
      x: this.#axis === 'x' ? this.canvas.size().width / 2 : this.canvas.size().width,
      y: this.#axis === 'x' ? this.canvas.size().height : this.canvas.size().height / 2,
    };

    new ShapeFactory(this.p5, {
      type: 'Line',
      isStrokeDashed: true,
      strokeWeight: 2,
      stroke: 'gray'
    }).drawShape(from, to);
  }

  /**
   * Adds a button and click handler to the options area
   * When clicked, toggle the line of symmetry
   * between horizontal to vertical
   */
  populateOptions() {
    super.populateOptions();
    this.toolOptions.addButton('Make Horizontal');

    /**
     * Click handler arrow function
     * When toggled, changes:
     * - the button label
     * - axis string
     * - line of symmetry value
     */
    const [button] = this.toolOptions.getList();
    button.changed(() => {
      if (this.#axis === 'x') {
        this.#axis = 'y';
        this.#lineOfSymmetry = this.p5.height / 2;
        button.rename('Make Vertical');
      } else {
        this.#axis = 'x';
        this.#lineOfSymmetry = this.p5.width / 2;
        button.rename('Make Horizontal');
      }
    });
  }

  /**
   * Calculate an opposite coordinate the other side of the symmetry line
   * @param n {Number} location for either x or y coordinate
   * @param a {String} the axis of the coordinate (x or y)
   * @returns {Number} the opposite coordinate
   */
  #calculateOpposite(n, a) {
    // if the axis isn't the one being mirrored return the same value
    if (a !== this.#axis) {
      return n;
    }

    /**
     * If n is less than the line of symmetry return a coordinate
     * that is far greater than the line of symmetry by the distance from
     * n to that line. Otherwise, a coordinate that is smaller than
     * the line of symmetry by the distance between it and n
     *   /
     */
    if (n < this.#lineOfSymmetry) {
      return this.#lineOfSymmetry + (this.#lineOfSymmetry - n);
    }

    return this.#lineOfSymmetry - (n - this.#lineOfSymmetry);
  }

  /**
   * Show the options specific to this tool, including the colour palette
   */
  showOptions() {
    super.showOptions();
    this.instance.colourPalette.show();
  }
}
