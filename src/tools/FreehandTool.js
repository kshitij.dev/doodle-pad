import BaseTool from './BaseTool.js';
import ShapeFactory from '../core/ShapeFactory.js';

/**
 * A tool that enables the user to draw freely by hand
 * @extends {BaseTool}
 */
export default class FreehandTool extends BaseTool {
  /**
   * Create an instance of FreehandTool
   */
  constructor(instance) {
    super(instance, 'Pencil', 'Free hand pencil', 'assets/pencil.svg');

    // Populate the tool options and hide them by default
    this.populateOptions();
    this.toolOptions.hideAll();
  }

  /**
   * Overrides the base method to draw using free hand strokes
   * Handles both the first frame and subsequent frames of drawing
   */
  drawMousePressed() {
    // Check if it's the first frame
    if (this.inputDevice.isFirstFrame()) {
      // Toggle the drawing state on
      this.isDrawing = true;

      // Store the starting mouse position
      this.inputDevice.updateInitialPosition();
    } else {
      // Update the current position of the input device
      this.inputDevice.updateCurrentPosition();

      // Create and draw a line shape using ShapeFactory
      new ShapeFactory(this.p5, {
        type: 'Line',
        strokeWeight: this.instance.strokeWeightManager.getWeight(),
        stroke: this.instance.colourPalette.getSelectedColour()
      }).drawShape(this.inputDevice.getInitialPosition(), this.inputDevice.getCurrentPosition());

      // Update the starting mouse position for the next line segment
      this.inputDevice.updateInitialPosition();
    }
  }

  /**
   * Shows tool options along with the colour palette
   */
  showOptions() {
    super.showOptions();
    this.instance.colourPalette.show();
  }
}
