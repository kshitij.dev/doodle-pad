import BaseTool from './BaseTool.js';
import SquarePattern from '../patterns/SquarePattern.js';
import FlowerPetalsPattern from '../patterns/FlowerPetalsPattern.js';
import FabricPattern from '../patterns/FabricPattern.js';
import DiamondPattern from '../patterns/DiamondPattern.js';
import TenPrintPattern from '../patterns/TenPrintPattern.js';
import MondrianPattern from '../patterns/MondrianPattern.js';
import ShapeFactory from '../core/ShapeFactory.js';

/**
 * A tool that enables the user to create a variety of patterns
 * @extends {BaseTool}
 */
export default class PatternTool extends BaseTool {

  /**
   * The preview container div for pattern visualization
   * @type {p5.Element}
   */
  #previewDiv;

  /**
   * The graphics buffer used for masking
   * @type {p5.Graphics}
   */
  #mask;

  /**
   * The graphics buffer for previewing patterns
   * @type {p5.Graphics}
   */
  #preview;

  /**
   * The currently selected shape for pattern creation
   * @type {String}
   */
  #selectedShape;

  /**
   * An array to store dragged points for the 'Complex' shape
   * @type {p5.Vector[]}
   */
  #draggedPoints;

  /**
   * An array to hold instances of different patterns
   * @type {BasePattern[]}
   */
  #pats;

  /**
   * The index of the currently selected pattern
   * @type {Number}
   */
  #selectedPattern;

  /**
   * Create an instance of the PatternTool
   */
  constructor(instance) {
    super(instance, 'Pattern', 'Patterned shapes', 'assets/pattern.svg');

    // Initialise fields
    this.#selectedShape = 'Rectangle';
    this.#draggedPoints = [];
    this.#pats = [];
    this.#selectedPattern = 0;

    this.instance.graphicsBufferManager.addBuffer(
      'shape',
      this.canvas.size().width,
      this.canvas.size().height
    );
    this.instance.graphicsBufferManager.addBuffer(
      'mask',
      this.canvas.size().width,
      this.canvas.size().height
    );
    this.instance.graphicsBufferManager.addObserver(this);
    this.shapeBuffer = this.instance.graphicsBufferManager.getBuffer('shape');
    this.#mask = this.instance.graphicsBufferManager.getBuffer('mask');

    this.populateOptions();
    this.toolOptions.hideAll();
    this.hideOptions();
  }

  /**
   * Draws a shape when the mouse is pressed
   */
  drawMousePressed() {
    // If it's the first frame, store the initial mouse position
    if (this.inputDevice.isFirstFrame()) {
      // Change to drawing state
      this.isDrawing = true;

      // Record the start mouse position
      this.inputDevice.updateInitialPosition();

      // Save the current canvas state (without pushing to undo stack)
      this.p5.loadPixels();
    } else {
      // Already drawing, so load the last saved pixels
      this.p5.updatePixels();
      this.shapeBuffer.clear();

      this.inputDevice.updateCurrentPosition();
      new ShapeFactory(this.p5, {
        type: this.#selectedShape,
        strokeWeight: 0,
        fillColour: 'white',
        isFilled: true,
        stroke: this.instance.colourPalette.getSelectedColour(),
        buffer: this.shapeBuffer,
      }).drawShape(
        this.inputDevice.getInitialPosition(),
        this.inputDevice.getCurrentPosition(),
        this.#draggedPoints
      );

      new ShapeFactory(this.p5, {
        type: this.#selectedShape,
        strokeWeight: this.instance.strokeWeightManager.getWeight(),
        stroke: this.instance.colourPalette.getSelectedColour(),
      }).drawShape(
        this.inputDevice.getInitialPosition(),
        this.inputDevice.getCurrentPosition(),
        this.#draggedPoints
      );

      const img = this.#mask.get();
      img.mask(this.shapeBuffer);
      this.p5.image(img, 0, 0);

      /**
       * Note: When the mouse is released, the pixels will be saved
       * in the undo stack by the base class method drawMouseFree
       */
    }
  }

  /**
   * Updates the pattern using the provided buffer
   * @param {p5.Graphics} buffer - The buffer to update the pattern with
   */
  updatePattern(buffer) {
    if (buffer === undefined) return;
    buffer.clear();
    this.#pats[this.#selectedPattern].generate();
  }

  /**
   * Adds tool-specific options buttons and their click handlers
   */
  populateOptions() {
    super.populateOptions();
    this.toolOptions.addSelect('Shape', ['Ellipse', 'Rectangle', 'Triangle', 'Complex']);
    this.toolOptions.addSelect('Pattern', this.createPatterns());
    this.addToolOptionCallbacks();
    this.showOptions();
  }

  /**
   * Creates and initialises pattern instances
   * @returns {String[]} - An array of pattern names
   */
  createPatterns() {
    this.#previewDiv = this.p5.createDiv('Preview');
    this.#previewDiv.parent(this.p5.select('.options'));

    this.#preview = this.p5.createGraphics(
      this.#previewDiv.size().width / 1.25,
      this.#previewDiv.size().height * 4
    );

    this.#preview.parent(this.#previewDiv);
    this.#mask.parent(this.p5.select('.content'));

    this.#preview.style('padding', '20px');
    this.#preview.show();

    this.#pats.push(new FlowerPetalsPattern(this.p5, this.#mask, this.#preview, this.#previewDiv));
    this.#pats.push(new MondrianPattern(this.p5, this.#mask, this.#preview, this.#previewDiv));
    this.#pats.push(new TenPrintPattern(this.p5, this.#mask, this.#preview, this.#previewDiv));
    this.#pats.push(new SquarePattern(this.p5, this.#mask, this.#preview, this.#previewDiv));
    this.#pats.push(new FabricPattern(this.p5, this.#mask, this.#preview, this.#previewDiv));
    this.#pats.push(new DiamondPattern(this.p5, this.#mask, this.#preview, this.#previewDiv));

    const patternNames = [];
    this.#pats.forEach((item) => {
      patternNames.push(item.getName());
    });
    return patternNames;
  }

  /**
   * Adds callbacks for tool options
   */
  addToolOptionCallbacks() {
    const [shape, pattern] = this.toolOptions.getList();

    shape.selected(this.#selectedShape);
    shape.changed(() => {
      this.#selectedShape = shape.value();
    });

    pattern.selected(this.#selectedPattern);
    pattern.changed(() => {
      this.#pats[this.#selectedPattern].hideOptions();
      this.#selectedPattern = this.#pats.findIndex((element) => {
        return pattern.value().replaceAll(' ', '') === element.getName().replaceAll(' ', '');
      });
      this.#pats[this.#selectedPattern].showOptions();
      this.updatePattern(this.#mask);
    });

    this.updatePattern(this.#mask);
    this.#pats.forEach((p) => p.hideOptions());
    this.#previewDiv.hide();
  }

  /**
   * Cleans up tool-specific items when the user selects another tool
   */
  unselectTool() {
    super.unselectTool();
    this.hideOptions();
  }

  /**
   * Hides the tool specific options
   */
  hideOptions() {
    this.#pats.forEach((pattern) => pattern.hideOptions());
    this.#previewDiv.hide();
  }

  /**
   * Handles mouse dragging, specifically for the 'Complex' shape
   */
  mouseDragged() {
    if (this.#selectedShape !== 'Complex') return;

    // Reset dragged points if first frame
    if (this.inputDevice.isFirstFrame()) {
      this.#draggedPoints = [];
    }

    this.inputDevice.updateCurrentPosition();

    // Ignore points outside the canvas and limit the number of dragged points per drag
    const maxDraggedPoints = 10000;
    if (this.inputDevice.isInCanvas() && this.#draggedPoints.length < maxDraggedPoints) {
      this.#draggedPoints.push(this.inputDevice.getCurrentPosition());
    }
  }

  /**
   * Handles freeing the mouse, cleaning up dragged points
   */
  drawMouseFree() {
    if (this.isDrawing) {
      this.#draggedPoints = [];
    }
    super.drawMouseFree();
  }

  /**
   * Show the options specific to this tool
   */
  showOptions() {
    super.showOptions();
    this.instance.colourPalette.show();
    this.#previewDiv.show();
    this.#pats[this.#selectedPattern].showOptions();
  }

  /**
   * Responds to GraphicsBufferManager notification (window resizing)
   */
  respondToGraphicsBufferManagerNotification() {
    this.updatePattern(this.#mask);
  }
}
