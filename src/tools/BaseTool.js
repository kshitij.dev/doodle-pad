/* eslint-disable class-methods-use-this */
import ToolOptionsManager from '../managers/ToolOptionsManager.js';
import Mouse from '../ui/Mouse.js';

/**
 * Base class which all tools inherit from
 */
export default class BaseTool {
  /**
   * The name of the tool
   * @type {String}
   */
  name;

  /**
   * The tooltip text of the tool
   * @type {String}
   */
  toolTip;

  /**
   * The filename of the tool's icon
   * @type {String}
   */
  icon;

  /**
   * Indicates whether the tool is currently in the drawing state
   * @type {Boolean}
   */
  isDrawing;

  /**
   * The instance object
   */
  instance;

  /**
   * The p5 instance of the current sketch
   */
  p5;

  /**
   * Object containing the canvas properties
   */
  canvas;

  /**
   * Instance of ToolOptionsManager, responsible for managing tool options
   */
  toolOptions;

  /**
   * The user input device for this tool
   * @type {InputDevice}
   */
  inputDevice;

  /**
   * Creates an instance of BaseTool
   * @param {Object} instance - The instance of the sketch
   * @param {String} name - The name of the tool
   * @param {String} toolTip - The tooltip text of the tool
   * @param {String} icon - The filename of the tool's icon
   */
  constructor(instance, name, toolTip, icon) {
    // Initialise properties
    this.name = name;
    this.toolTip = toolTip;
    this.icon = icon;
    this.instance = instance;
    this.p5 = instance.p5;
    this.isDrawing = false;
    this.canvas = instance.canvas;
    this.toolOptions = new ToolOptionsManager(this.p5);
    this.inputDevice = new Mouse(instance.p5, this.canvas);
  }

  /**
   * Indirectly called by the p5 draw function
   * Depending on mouse pressed a different draw action is performed
   */
  draw() {
    if (this.inputDevice.isInCanvas() && this.inputDevice.isPressed()) {
      this.drawMousePressed();
    } else {
      this.drawMouseFree();
    }
  }

  /**
   * Called in the draw loop while the mouse is pressed
   */
  drawMousePressed() {
    /**
     *  This is the base method. No actions necessary by default
     *  We also don't need to add an exception as some tools
     *  may not need a draw action while the mouse is pressed
     */
  }

  /**
   * Called in the draw loop when the mouse is not pressed
   */
  drawMouseFree() {
    // Action performed only if the user was previously drawing
    if (this.isDrawing) {
      // Reset drawing state
      this.isDrawing = false;
      // Save the current canvas to the pixel array
      this.p5.loadPixels();
      // Reset mouse start
      this.inputDevice.resetState();
      // Save the current state of the canvas to the undo stack
      this.instance.historyManager.addToUndoStack();
    }
    // Otherwise, no draw action is necessary
  }

  /**
   * When the tool is deselected, reset any drawing aids and clear options
   */
  unselectTool() {
    // Draw the last item from the undo stack
    this.instance.historyManager.redraw();

    // Clear options
    this.toolOptions.hideAll();
    this.instance.colourPalette.hide();
  }

  /**
   * Called by the p5.js mouseDragged function via the Toolbox
   */
  mouseDragged() {
    /**
     * This is the base method
     * No exceptions as most tools don't respond to a mouse dragged
     */
  }

  /**
   * Called by the p5.js mousePressed function via the Toolbox
   */
  mousePressed() {
    /**
     * This is the base method
     * No exceptions as most tools don't respond to a mouse pressed
     */
  }

  /**
   * Called by the p5.js mouseReleased function via the Toolbox
   */
  mouseReleased() {
    /**
     * This is the base method
     * No exceptions as most tools don't respond to a mouse released
     */
  }

  /**
   * Populates tool options
   */
  populateOptions() { }

  /**
   * Shows tool options
   */
  showOptions() {
    this.toolOptions.showAll();
  }

  /**
   * Responds to GraphicsBufferManager notification
   */
  respondToGraphicsBufferManagerNotification() { }
}
