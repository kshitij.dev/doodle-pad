import BaseTool from './BaseTool.js';
import ShapeFactory from '../core/ShapeFactory.js';

/**
 * A tool that allows the user to create editable shapes
 * @extends {BaseTool}
 */
export default class EditableShapeTool extends BaseTool {
  /**
   * Indicates whether the tool is currently in edit mode
   * @type {Boolean}
   */
  #inEditMode;

  /**
   * The index of the point currently being edited by the user
   * @type {Number}
   */
  #indexCurrentlyEditing;

  /**
   * An array containing the coordinates of all points
   * @type {p5.Vector[]}
   */
  #points;

  /**
   * The ID of the timer
   * @type {Number}
   */
  #timer;

  /**
   * Tool-specific buttons
   * @type {Object}
   */
  buttons;

  /**
   * Create an instance of EditableShapeTool
   */
  constructor(instance) {
    super(instance, 'Editable', 'Editable free-hand', 'assets/editableShape.svg');

    // Initialise all fields
    this.#indexCurrentlyEditing = undefined;
    this.#inEditMode = false;
    this.#points = [];
    this.buttons = {
      edit: {}, create: {}, finish: {},
    };

    this.populateOptions();
    this.toolOptions.hideAll();
  }

  /**
   * Draws the editable shape when the mouse is pressed
   */
  drawMousePressed() {
    // Toggle drawing mode on
    this.isDrawing = true;

    // Apply previously saved pixels onto the canvas
    this.p5.updatePixels();

    // Disable history manager buttons
    this.instance.historyManager.disableAllButtons();

    // Enable the tool-specific options
    this.#enableOptions();

    // If not in edit mode, add new point coordinates; otherwise, edit them
    if (!this.#inEditMode) {
      this.inputDevice.updateCurrentPosition();
      this.#points.push(this.inputDevice.getCurrentPosition().copy());
    } else {
      this.#editPointCoords();
    }

    // Loop over all points and draw the shape
    this.#drawShape();
  }

  /**
   * Overrides the base drawMouseFree function
   */
  drawMouseFree() {
    // Apply any stored pixels
    this.p5.updatePixels();

    // Redraw the shape
    this.#drawShape();

    // If in edit mode, reset the currently editing index and clear the timer
    if (this.#inEditMode) {
      this.#indexCurrentlyEditing = undefined;
      clearTimeout(this.#timer);
    }
  }

  /**
   * Adds tool-specific options buttons and their click handlers
   */
  populateOptions() {
    super.populateOptions();

    this.toolOptions.addButton('Edit Shape');
    this.toolOptions.addButton('Finish Shape');
    this.toolOptions.addButton('Clear Shape');
    [this.buttons.edit, this.buttons.finish, this.buttons.clear] = this.toolOptions.getList();

    this.buttons.edit.changed(() => {
      // Check if user is editing
      if (this.#inEditMode) {
        // Toggle edit mode off
        this.#inEditMode = false;

        // Update the button label
        this.buttons.edit.rename('Edit Shape');

        // As user has just started editing, loose any old timers
        clearTimeout(this.#timer);

        // Apply the last stored pixel array onto canvas
        this.p5.updatePixels();
      } else {
        /**
         * Remove duplicate points that the user may have drawn
         * if they were slow to move their mouse around
         * while drawing
         */
        this.#cleanupDuplicates();

        // Set edit mode to on so that draw loop reacts to it
        this.#inEditMode = true;

        // Change the label of the edit button
        this.buttons.edit.rename('Add Vertices');
      }
    });

    this.buttons.clear.changed(() => {
      // Reset edit mode
      this.#inEditMode = false;

      // Toggle drawing state to off
      this.isDrawing = false;

      // Reset private variables and timer
      this.#indexCurrentlyEditing = undefined;
      this.#points = [];
      clearTimeout(this.#timer);

      // Apply the last saved pixel from the undo stack
      this.instance.historyManager.redraw();

      // Disable buttons until user starts drawing again
      this.#disableOptions();

      // Update the button labels for next drawing
      this.buttons.edit.rename('Edit Shape');
    });

    // Finish shape click handler arrow function
    this.buttons.finish.changed(() => {
      // As user finished editing, clear the timeout
      clearTimeout(this.#timer);

      // Reset edit mode
      this.#inEditMode = false;

      // Force a redraw of the editable shape
      this.draw();

      // Save the pixels after the redraw
      this.p5.loadPixels();

      // Reset private variables
      this.#indexCurrentlyEditing = undefined;
      this.#points = [];

      // Reset start mouse position
      this.inputDevice.resetState();

      // Toggle drawing state off
      this.isDrawing = false;

      // Push pixels to the undo stack and redraw the image
      this.instance.historyManager.addToUndoStack();
      this.instance.historyManager.redraw();

      // Update the button labels for next drawing
      this.buttons.edit.rename('Edit Shape');

      // Disable buttons until user starts drawing
      this.#disableOptions();
    });

    // Deactivate buttons by default
    this.#disableOptions();
  }

  /**
   * Finds the points near the mouse and updates their position
   */
  #editPointCoords() {
    this.inputDevice.updateCurrentPosition();
    const { x, y } = this.inputDevice.getCurrentPosition();

    // If the timer is still running, stick to the currently editing point
    if (this.#indexCurrentlyEditing !== undefined) {
      this.#points[this.#indexCurrentlyEditing].set(x, y);
      return;
    }

    // Find and update the closest point
    const closestIndex = this.#findClosestPoint();
    this.#updatePositionOfPoint(closestIndex, x, y);
  }

  /**
   * Updates the position of a specific point if conditions are met
   * @param {Number} index - The index of the point to update
   * @param {Number} newX - The new x-coordinate for the point
   * @param {Number} newY - The new y-coordinate for the point
   */
  #updatePositionOfPoint(index, newX, newY) {
    // Check if the timer is running and if the index is valid
    if (this.#indexCurrentlyEditing === undefined && index !== undefined) {
      // Assign the provided index to the currently editing index
      this.#indexCurrentlyEditing = index;

      // Update the coordinates of the point with new values
      this.#points[this.#indexCurrentlyEditing].set(newX, newY);

      // Start the timer to ensure the pressed mouse sticks to the point
      this.#startTimer();
    }
  }

  /**
   * Finds the point nearest to the current mouse position
   * @returns {Number} Index of the point
   */
  #findClosestPoint() {
    const closePoints = [];
    let closest;

    // Loop over all points and find all points within 15 pixels of mouse
    for (let index = 0; index < this.#points.length; index++) {
      const d = this.#points[index].dist(this.inputDevice.getCurrentPosition());

      // Only push to close points if within 15px
      if (d < 15) {
        closePoints.push({ index, distance: d });
      }

      // Check if the user clicked within 15px of any points
      if (closePoints.length > 0) {
        // If they did, sort the points on distance (ascending)
        closePoints.sort((first, second) => first.distance - second.distance);

        // Choose the closest point as the currently editing point
        closest = closePoints[0].index;
      }
    }
    return closest;
  }

  /**
   * Removes any neighbouring points that are within 1 px of another point
   */
  #cleanupDuplicates() {
    // Check if there are any points drawn by the user
    if (this.#points.length > 0) {
      // Create a set to keep a log of UNIQUE indices to be removed
      const indicesToRemove = new Set();

      // Loop over all points
      for (let i = 0; i < this.#points.length; ++i) {
        // For each point, loop over all points again
        for (let j = i + 1; j < this.#points.length; ++j) {
          // calculate the distance between this point and other points
          const d = this.#points[j].dist(this.#points[i]);

          /**
           * If the points are not the same, are neighboring
           * and within 1px then mark them for removal
           */
          if (d <= 1 && Math.abs(i - j) <= 2 && !(indicesToRemove.has(i)
            || indicesToRemove.has(j))) {
            indicesToRemove.add(i);
          }
        }
      }

      // Convert the set of indices to an array and sort it in descending order,
      // so that we can remove back-to-font
      const indicesToRemoveArray = Array.from(indicesToRemove).sort((a, b) => b - a);

      // Remove the marked points from the array
      indicesToRemoveArray.forEach((index) => {
        this.#points.splice(index, 1);
      });
    }
  }

  /**
   * Draws the editable shape
   */
  #drawShape() {
    // Check if there is anything to be drawn
    if (this.#points.length <= 0) return;

    new ShapeFactory(this.p5, {
      type: 'Complex',
      stroke: this.instance.colourPalette.getSelectedColour(),
      strokeWeight: this.instance.strokeWeightManager.getWeight(),
      isFilled: false,
      isClosed: false
    }).drawShape(
      this.inputDevice.getInitialPosition(),
      this.inputDevice.getCurrentPosition(),
      this.#points
    );

    this.#drawEditingAid();
  }

  /**
   * Draws ellipses at each point when in edit mode
   */
  #drawEditingAid() {
    // If in edit mode, draw an ellipse on each point
    if (this.#inEditMode) {
      const aidSize = Math.max(10, this.instance.strokeWeightManager.getWeight());

      // Loop over each point and draw an ellipse
      for (let index = 0; index < this.#points.length; index++) {
        /**
         * Determine the coordinates of the rectangle for the shape factory,
         * which expects the top-left and bottom-right coordinates
         */
        const from = this.#points[index].copy();
        const to = (from.copy()).add(aidSize, aidSize);

        // Create and draw an ellipse using ShapeFactory
        new ShapeFactory(this.p5, {
          type: 'Ellipse',
          stroke: this.instance.colourPalette.getSelectedColour(),
          strokeWeight: 1,
        }).drawShape(from, to);
      }
    }
  }

  /**
   * Sets a 5-second timeout before resetting the currently editing index
   */
  #startTimer() {
    this.#timer = setTimeout(() => {
      this.#indexCurrentlyEditing = undefined;
    }, 5000);
  }

  /**
   * Cleans up tool-specific items if the user selects another tool
   */
  unselectTool() {
    // Reset edit mode to off
    this.#inEditMode = false;

    // Draw the editable shape without ellipses (as not in edit mode)
    this.draw();

    // Update the pixel array
    this.p5.loadPixels();

    // If user was drawing, push the canvas to the undo stack
    if (this.isDrawing) {
      this.instance.historyManager.addToUndoStack();
    }

    // Call the base class method
    super.unselectTool();

    // Reset the currently editing index
    this.#indexCurrentlyEditing = undefined;

    // Clear timeout and coordinate arrays
    clearTimeout(this.#timer);
    this.#points = [];

    // reset options
    this.#disableOptions();
  }

  /**
   * Enables the tool-specific buttons
   */
  #enableOptions() {
    this.buttons.edit.enable();
    this.buttons.finish.enable();
    this.buttons.clear.enable();
  }

  /**
   * Disables the tool-specific buttons
   */
  #disableOptions() {
    this.buttons.edit.disable();
    this.buttons.finish.disable();
    this.buttons.clear.disable();
  }

  /**
   * Shows tool options, including colour palette
   */
  showOptions() {
    super.showOptions();
    this.instance.colourPalette.show();
  }
}
