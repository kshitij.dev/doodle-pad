import ButtonProxy from '../proxies/ButtonProxy.js';
import InputProxy from '../proxies/InputProxy.js';

/**
 * Manages the saving and naming of the drawing
 */
export default class SaveFileManager {
  #prevCanvasTitle;

  /**
   * Creates a new instance of SaveFileManager
   * @param {p5} p5 - The p5 instance
   */
  constructor(p5) {
    /**
     * The previous title of the canvas
     * @type {string}
     */
    this.#prevCanvasTitle = 'My Doodle';

    // Create the DOM objects required by the save manager
    const div = p5.select('#header');

    // Save file text input
    const saveFileInput = new InputProxy(p5, div).create('', this.#prevCanvasTitle, div);

    // Save button
    const saveButton = new ButtonProxy(p5, div).create('Save', div);

    // Click handler for the save image button, saves the canvas to the local file system
    saveButton.changed(() => {
      p5.saveCanvas(saveFileInput.value());
    });

    // Validator for the drawing title
    saveFileInput.changed(() => {
      const newValue = saveFileInput.value();
      const isValid = /^[a-zA-Z0-9_\-.\s]+$/.test(newValue);
      if (isValid) {
        this.#prevCanvasTitle = newValue;
      } else {
        alert(`"${saveFileInput.value()}" is an invalid title.`);
        saveFileInput.value(this.#prevCanvasTitle);
      }
    });
  }
}
