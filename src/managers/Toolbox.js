import FreehandTool from '../tools/FreehandTool.js';
import LineToTool from '../tools/LineToTool.js';
import GeometricalShapesTool from '../tools/GeometricalShapesTool.js';
import EditableShapeTool from '../tools/EditableShapeTool.js';
import ScissorTool from '../tools/ScissorTool.js';
import BucketTool from '../tools/BucketTool.js';
import SprayCanTool from '../tools/SprayCanTool.js';
import MirrorDrawTool from '../tools/MirrorDrawTool.js';
import EraserTool from '../tools/EraserTool.js';
import PatternTool from '../tools/PatternTool.js';

/**
 * Manages the state of various tools and their selection
 */
export default class Toolbox {
  /**
   * The p5 sketch instance associated with this toolbox
   * @type {p5}
   */
  #p5;

  /**
   * Array containing all available tools
   * @type {BaseTool[]}
   */
  #tools = [];

  /**
   * The currently selected tool
   * @type {BaseTool}
   */
  #selectedTool = null;

  /**
   * Creates a new instance of Toolbox
   * @param {p5} p5 - The p5 instance
   */
  constructor(p5) {
    this.#p5 = p5;

    // Set the background to white
    this.#p5.background(255);
  }

  /**
   * Initialises the toolbox by adding all available tools
   * @param {Object} instance - The instance object
   */
  setup(instance) {
    this.#addTool(new FreehandTool(instance));
    this.#addTool(new LineToTool(instance));
    this.#addTool(new SprayCanTool(instance));
    this.#addTool(new MirrorDrawTool(instance));
    this.#addTool(new EditableShapeTool(instance));
    this.#addTool(new ScissorTool(instance));
    this.#addTool(new GeometricalShapesTool(instance));
    this.#addTool(new BucketTool(instance));
    this.#addTool(new EraserTool(instance));
    this.#addTool(new PatternTool(instance));
  }

  /**
   * Calls the draw method of the selected tool
   */
  draw() {
    this.#selectedTool.draw();
  }

  /**
   * Calls the mouseDragged method of the selected tool
   */
  mouseDragged() {
    this.#selectedTool.mouseDragged();
  }

  /**
   * Calls the mousePressed method of the selected tool
   */
  mousePressed() {
    this.#selectedTool.mousePressed();
  }

  /**
   * Calls the mouseReleased method of the selected tool
   */
  mouseReleased() {
    this.#selectedTool.mouseReleased();
  }

  /**
   * Handles the click event for selecting tools
   * @param {p5.Element} sideBarItem - The clicked sideBarItem
   */
  #toolbarItemClick(sideBarItem) {
    // Remove existing borders and backgrounds from all items
    const items = this.#p5.selectAll('.sideBarItem');
    for (let i = 0; i < items.length; i++) {
      items[i].style('border', '4px solid var(--secondary-background)');
      items[i].style('background', 'var(--secondary-background)');
    }

    // Identify the selected tool and apply styling
    const toolName = sideBarItem.id().split('sideBarItem')[0];
    this.#selectTool(toolName);

    // Call loadPixels to save recent changes to pixel array
    this.#p5.loadPixels();
  }

  /**
   * Adds an icon and tooltip for a new tool to the toolbox
   * @param {string} icon - Path to the tool's icon
   * @param {string} name - Name of the tool
   * @param {string} toolTip - Tooltip for the tool
   */
  #addToolIcon(icon, name, toolTip) {
    // Create an icon element for the tool
    const sideBarItem = this.#p5.createDiv(`<img src='${icon}' alt='${name}'></div>`);
    sideBarItem.class('sideBarItem');
    sideBarItem.id(`${name}sideBarItem`);
    sideBarItem.parent('leftSideBar');
    sideBarItem.style('border', '4px solid var(--secondary-background)');
    sideBarItem.style('background', 'var(--secondary-background)');

    // Create a tooltip element for the tool
    const span = this.#p5.createSpan(`${toolTip}`);
    span.parent(sideBarItem);
    span.class('tooltiptext');

    // Handle mouse click for tool selection
    sideBarItem.mouseClicked(() => {
      this.#toolbarItemClick(sideBarItem);
    });
  }

  /**
   * Adds a tool to the toolbox and sets it as the selected tool if none is selected
   * @param {BaseTool} tool - The tool to be added
   */
  #addTool(tool) {
    // Ensure the tool has an icon and a name
    if (!('icon' in tool) || !('name' in tool)) {
      alert('Make sure your tool has both a name and an icon.');
    }

    // Add the tool to the list of tools
    this.#tools.push(tool);

    // Add the tool's icon and tooltip to the toolbox
    this.#addToolIcon(tool.icon, tool.name, tool.toolTip);

    // If no tool is selected, make this tool the selected one
    if (this.#selectedTool == null) {
      this.#selectTool(tool.name);
    }
  }

  /**
   * Selects a tool by its name and updates the toolbar's appearance
   * @param {string} toolName - The name of the tool to select
   */
  #selectTool(toolName) {
    // Find the tool with the given name
    for (let i = 0; i < this.#tools.length; i++) {
      if (this.#tools[i].name === toolName) {
        // Unselect the previously selected tool if it has an unselectTool method
        if (this.#selectedTool != null && 'unselectTool' in this.#selectedTool) {
          this.#selectedTool.unselectTool();
        }

        // Select the new tool and highlight it on the toolbar
        this.#selectedTool = this.#tools[i];
        this.#p5
          .select(`#${toolName}sideBarItem`)
          .style('border', '4px solid var(--secondary-variant-background)')
          .style('background', 'var(--secondary-variant-background)');
        this.#selectedTool.showOptions();
      }
    }
  }
}
