import SliderProxy from '../proxies/SliderProxy.js';
import ColourPickerProxy from '../proxies/ColourPickerProxy.js';
import ButtonProxy from '../proxies/ButtonProxy.js';
import InputProxy from '../proxies/InputProxy.js';
import SelectProxy from '../proxies/SelectProxy.js';
import CheckboxProxy from '../proxies/CheckboxProxy.js';

/**
 * Manages tool options most of which are proxies of p5.DOM elements
 */
export default class ToolOptionsManager {
  /**
   * The list of tool options
   * @type {BaseProxy[]}
   */
  #list;

  /**
   * The p5 sketch instance associated with this manager
   * @type {p5}
   */
  #p5;

  /**
   * The DOM element for tool options buttons
   * @type {p5.Element}
   */
  #toolOptionsButtonsDiv;

  /**
   * The DOM element for tool options menus
   * @type {p5.Element}
   */
  #toolOptionsMenusDiv;

  /**
   * Creates a new ToolOptionsManager instance
   * @param {p5} p5 - The p5 instance
   */
  constructor(p5) {
    /**
     * The list of tool options
     * @type {BaseProxy[]}
     */
    this.#list = [];

    /**
     * The p5 instance associated with this manager
     * @type {p5}
     */
    this.#p5 = p5;

    /**
     * The DOM element for tool options buttons
     * @type {p5.Element}
     */
    this.#toolOptionsButtonsDiv = this.#p5.select('.subOptionsButtons');

    /**
     * The DOM element for tool options menus
     * @type {p5.Element}
     */
    this.#toolOptionsMenusDiv = this.#p5.select('.subOptionsMenus');
  }

  /**
   * Adds a slider option to the list of tool options
   * @param {String} name - The name of the slider
   * @param {Number} min - The minimum value of the slider
   * @param {Number} max - The maximum value of the slider
   * @param {Number} value - The initial value of the slider
   * @param {Number} step - The step size of the slider
   */
  addSlider(name, min, max, value, step) {
    this.#list.push(new SliderProxy(this.#p5, this.#toolOptionsMenusDiv)
      .create(name, min, max, value, step));
  }

  /**
   * Adds a colour picker option to the list of tool options
   * @param {String} name - The name of the colour picker
   * @param {String} value - The initial colour value of the picker
   */
  addColourPicker(name, value) {
    this.#list.push(new ColourPickerProxy(this.#p5, this.#toolOptionsMenusDiv)
      .create(name, value));
  }

  /**
   * Adds a button option to the list of tool options
   * @param {String} name - The name of the button
   */
  addButton(name) {
    this.#list.push(new ButtonProxy(this.#p5, this.#toolOptionsButtonsDiv)
      .create(name));
  }

  /**
   * Adds an input option to the list of tool options
   * @param {String} name - The name of the input
   * @param {String} value - The initial value of the input
   */
  addInput(name, value) {
    this.#list.push(new InputProxy(this.#p5, this.#toolOptionsMenusDiv)
      .create(name, value));
  }

  /**
   * Adds a select dropdown option to the list of tool options
   * @param {String} name - The name of the select dropdown
   * @param {String[]} list - The list of options for the dropdown
   */
  addSelect(name, list) {
    this.#list.push(new SelectProxy(this.#p5, this.#toolOptionsButtonsDiv)
      .create(name, list));
  }

  /**
   * Adds a checkbox option to the list of tool options
   * @param {String} name - The name of the checkbox
   * @param {Boolean} value - The initial value of the checkbox
   */
  addCheckbox(name, value) {
    this.#list.push(new CheckboxProxy(this.#p5, this.#toolOptionsMenusDiv)
      .create(name, value));
  }

  /**
   * Hides all tool options
   */
  hideAll() {
    if (this.#list.length > 0) {
      this.#list.forEach((option) => option.hide());
    }
  }

  /**
   * Shows all tool options
   */
  showAll() {
    if (this.#list.length > 0) {
      this.#list.forEach((option) => option.show());
    }
  }

  /**
   * Attaches a common change handler function to all tool options
   * @param {Function} func - The change handler function
   */
  changedAll(func) {
    if (this.#list.length > 0) {
      this.#list.forEach((option) => option.changed(func));
    }
  }

  /**
   * Gets the list of tool options
   * @returns {BaseProxy[]} - The list of tool options
   */
  getList() {
    return this.#list;
  }
}
