import ButtonProxy from '../proxies/ButtonProxy.js';

/**
 * Manages the history of p5.Image objects and provides undo and redo functionality
 */
export default class HistoryManager {
  /**
   * p5 instance
   * @type {p5}
   */
  #p5;

  /**
   * Array containing a list of p5.Image objects for undo
   * @type {p5.Image[]}
   */
  #undoStack = [];

  /**
   * Array containing p5.Image objects that have been undone and can be redone
   * @type {p5.Image[]}
   */
  #redoStack = [];

  /**
   * ButtonProxy instance for the "Undo" button
   * @type {ButtonProxy}
   */
  #undoButton;

  /**
   * ButtonProxy instance for the "Redo" button
   * @type {ButtonProxy}
   */
  #redoButton;

  /**
   * ButtonProxy instance for the "Clear" button
   * @type {ButtonProxy}
   */
  #clearButton;

  /**
   * Creates a new HistoryManager instance
   * @param {p5} p5 - The p5 instance
   */
  constructor(p5) {
    this.#p5 = p5;
    const div = this.#p5.select('#header');

    // Create the DOM objects required by the history manager
    this.#undoButton = new ButtonProxy(this.#p5, div).create('Undo', div);
    this.#redoButton = new ButtonProxy(this.#p5, div).create('Redo', div);
    this.#clearButton = new ButtonProxy(this.#p5, div).create('Clear', div);
    this.updateButtonsState();

    // Add the click events for the three buttons
    this.#undoButton.changed(() => { this.undo(); });
    this.#redoButton.changed(() => { this.redo(); });
    this.#clearButton.changed(() => {
      this.#p5.background(255, 255, 255);
      this.addToUndoStack();
      this.redraw();
    });
  }

  /**
   * Adds the current state (p5.Image) to the undo stack
   */
  addToUndoStack() {
    this.#redoStack = [];
    this.#undoStack.push(this.#p5.get());
    this.updateButtonsState();
  }

  /**
   * Moves the last p5.Image from the undo stack to the redo stack and calls the redraw method
   */
  undo() {
    if (this.#undoStack.length > 0) {
      const lastDrawnObject = this.#undoStack.pop();
      this.#p5.background(255);
      this.#redoStack.push(lastDrawnObject);
      this.redraw();
    }
  }

  /**
   * If the redo stack has any p5.Image objects,
   * it moves the last one from the redo stack to the undo stack and calls the redraw method
   */
  redo() {
    if (this.#redoStack.length > 0) {
      const redoObject = this.#redoStack.pop();
      this.#undoStack.push(redoObject);
      this.redraw();
    }
  }

  /**
   * Applies the last p5.Image in the undo stack
   * Also updates the active state of buttons depending on the size of the undo and redo arrays
   */
  redraw() {
    this.#p5.background(255);
    if (this.#undoStack.length > 0) {
      this.#p5.set(0, 0, this.#undoStack[this.#undoStack.length - 1]);
    }

    // Update pixel array for tools that update pixels as the first draw step
    this.#p5.loadPixels();

    this.updateButtonsState();
  }

  /**
   * Updates the enabled/disabled state of the buttons based on the undo and redo stacks
   */
  updateButtonsState() {
    if (this.#undoStack.length > 0) {
      this.#undoButton.enable();
      this.#clearButton.enable();
    } else {
      this.#undoButton.disable();
      this.#clearButton.disable();
    }

    if (this.#redoStack.length > 0) {
      this.#redoButton.enable();
    } else {
      this.#redoButton.disable();
    }
  }

  /**
   * Disables all buttons
   * TODO: A future enhancement would be to enable undoing editable shapes while editing
   */
  disableAllButtons() {
    this.#undoButton.disable();
    this.#clearButton.disable();
    this.#redoButton.disable();
  }
}
