import SliderProxy from '../proxies/SliderProxy.js';

/**
 * Manages the state of stroke weight slider
 */
export default class StrokeWeightManager {
  /**
   * The current stroke weight
   * @type {Number}
   */
  #weight;

  /**
   * The slider element for controlling stroke weight
   * @type {SliderProxy}
   */
  #slider = undefined;

  /**
   * p5 instance
   * @type {p5}
   */
  #p5;

  /**
   * Creates a new StrokeWeightManager instance
   * @param {p5} p5 - The p5 instance
   */
  constructor(p5) {
    this.#weight = 2;
    this.#p5 = p5;

    // Create the slider element for stroke weight control
    this.#slider = new SliderProxy(this.#p5, this.#p5.select('.bottomPanel'))
      .create('Stroke Weight', 1, 20, this.#weight, 1);
  }

  /**
   * Returns the current stroke weight
   * @returns {number} The current stroke weight
   */
  getWeight() {
    return this.#slider.value();
  }

  /**
   * Shows the slider element for stroke weight
   */
  show() {
    this.#slider.show();
  }

  /**
   * Hides the slider element for stroke weight
   */
  hide() {
    this.#slider.hide();
  }
}
