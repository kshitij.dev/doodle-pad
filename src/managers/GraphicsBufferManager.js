/**
 * Manages graphics buffers and observers for notifications
 */
export default class GraphicsBufferManager {
  /**
   * p5 instance
   * @type {p5}
   */
  #p5;

  /**
   * Map to store graphic buffers
   * @type {Map<String, #p5.Graphics>}
   */
  #buffers;

  /**
   * Array to hold observer objects
   * @type {Array}
   */
  #observers;

  /**
   * Creates a GraphicsBufferManager instance
   * @param {p5} p5 - The p5 instance
   */
  constructor(p5) {
    this.#p5 = p5;
    this.#buffers = new Map();
    this.#observers = [];
  }

  /**
   * Adds a new graphics buffer to the manager
   * @param {String} name - The name of the buffer
   * @param {Number} w - The width of the buffer
   * @param {Number} h - The height of the buffer
   */
  addBuffer(name, w, h) {
    this.#buffers.set(name, this.#p5.createGraphics(w, h));
  }

  /**
   * Adds an observer to the manager
   * @param {Object} observer - The observer object
   */
  addObserver(observer) {
    this.#observers.push(observer);
  }

  /**
   * Notifies all observers of a change
   */
  notifyObservers() {
    this.#observers.forEach((observer) => {
      observer.respondToGraphicsBufferManagerNotification();
    });
  }

  /**
   * Retrieves a graphics buffer by its name
   * @param {String} name - The name of the buffer to retrieve
   * @returns {#p5.Graphics} The requested graphics buffer
   */
  getBuffer(name) {
    return this.#buffers.get(name);
  }

  /**
   * Resizes all graphics buffers and notifies observers
   * @param {Number} w - The new width for the buffers
   * @param {Number} h - The new height for the buffers
   */
  resizeAll(w, h) {
    this.#buffers.forEach((buffer) => {
      buffer.resizeCanvas(w, h);
    });
    this.notifyObservers();
  }
}
