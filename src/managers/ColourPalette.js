// This class represents a colour palette and provides methods to display and handle colours

export default class ColourPalette {
  //
  /**
   * List of colour strings
   * @type {String[]}
   */
  #colours;

  /**
   * p5 instance
   * @type {p5}
   */
  #p5;

  /**
   * The instance object
   */
  #instance;

  /**
   * Currently selected colour
   * @type {String}
   */
  #selectedColour;

  // Constructor for the ColourPalette class
  constructor(p5) {
    // List of predefined colour strings
    this.#colours = [
      'black', 'silver', 'grey', 'white', 'maroon', 'red',
      'purple', 'orange', 'pink', 'fuchsia', 'green', 'lime',
      'olive', 'yellow', 'navy', 'blue', 'teal', 'aqua'];

    // Set the selected colour to the first colour in the list
    [this.#selectedColour] = this.#colours;

    // Store the p5 instance
    this.#p5 = p5;

    // Load the colours and create colour swatches
    this.#loadColours();
  }

  // Returns the currently selected colour
  getSelectedColour() {
    return this.#selectedColour;
  }

  // Handles the click event on a colour swatch
  #colourClick(colourSwatch) {
    // Remove the old border from the previously selected colour swatch
    const current = this.#p5.select(`#${this.#selectedColour}Swatch`);
    current.style('border', '4px solid var(--secondary-background)');

    // Set the selected colour and update the border of the clicked swatch
    [this.#selectedColour] = colourSwatch.id().split('Swatch');
    colourSwatch.style('border', '4px solid var(--secondary-variant-background)');
  }

  // Show the colour palette
  show() {
    this.#p5.select('#pencilColour').show();
    this.#instance.strokeWeightManager.show();
  }

  // Hide the colour palette
  hide() {
    this.#p5.select('#pencilColour').hide();
    this.#instance.strokeWeightManager.hide();
  }

  // Load colours and create colour swatches
  #loadColours() {
    // Iterate over each colour and create a new div element for the colour swatch
    this.#colours.forEach((colour) => {
      const colourID = `${colour}Swatch`;

      // Create a new colour swatch div and set its class and id
      const colourSwatch = this.#p5.createDiv();
      colourSwatch.class('colourSwatches');
      colourSwatch.id(colourID);

      // Append the colour swatch to the palette and set its background colour
      this.#p5.select('.colourPalette').child(colourSwatch);
      this.#p5.select(`#${colourID}`).style('background-color', colour);

      // Add a click event listener to handle colour selection
      colourSwatch.mouseClicked(() => {
        this.#colourClick(colourSwatch);
      });
    });

    // Set a default border for colour swatches
    this.#p5
      .select('.colourSwatches')
      .style('border', '4px solid var(--secondary-variant-background)');
  }

  // Set the instance object associated with this palette
  setInstance(instance) {
    this.#instance = instance;
  }
}
