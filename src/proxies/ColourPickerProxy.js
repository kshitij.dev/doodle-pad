import BaseProxy from './BaseProxy.js';

/**
 * Represents a colour picker proxy class extending BaseProxy for managing colour pickers
 */
export default class ColourPickerProxy extends BaseProxy {
  /**
   * Creates a colour picker element
   * @param {String} label - The label for the colour picker
   * @param {String} value - The initial value (colour) of the picker
   * @returns {ColourPickerProxy} - The created ColourPickerProxy instance
   */
  create(label, value = 'black') {
    this.div = this.p5.createDiv('');
    this.div.parent(this.parent);

    this.element = this.p5.createColorPicker(value);
    this.element.parent(this.div);
    this.element.style('width', '80px');
    this.span = this.p5.createSpan(`&nbsp ${label}`);
    this.span.parent(this.div);

    return this;
  }

  /**
   * Assigns a function to be executed when the colour picker value changes
   * @param {Function} func - The function to be executed
   */
  changed(func) {
    if (this.element !== undefined) {
      this.element.input(func);
    }
  }
}
