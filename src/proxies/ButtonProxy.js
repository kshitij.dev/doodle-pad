import BaseProxy from './BaseProxy.js';

/**
 * Represents a button proxy class extending BaseProxy for managing buttons
 */
export default class ButtonProxy extends BaseProxy {
  /**
   * Creates a button element
   * @param {String} label - The label for the button
   * @param {p5.Element} div - The parent element for the button (optional)
   * @returns {ButtonProxy} - The created ButtonProxy instance
   */
  create(label, div = undefined) {
    if (div === undefined) {
      this.div = this.p5.createDiv('');
      this.div.parent(this.parent);
    } else {
      this.div = div;
    }

    this.element = this.p5.createButton(label);
    this.element.parent(this.div);
    this.span = this.p5.createSpan('');
    this.span.parent(this.div);

    return this;
  }

  /**
   * Assigns a function to be executed on button click
   * @param {Function} func - The function to be executed
   */
  changed(func) {
    if (this.element !== undefined) {
      this.element.mouseClicked(func);
    }
  }

  /**
   * Renames the button with a new label
   * @param {String} newName - The new label for the button
   */
  rename(newName) {
    if (this.element !== undefined) {
      this.element.html(newName);
    }
  }

  /**
   * Disables the button
   */
  disable() {
    if (this.element !== undefined) this.element.attribute('disabled', 'true');
  }

  /**
   * Enables the button
   */
  enable() {
    if (this.element !== undefined) this.element.removeAttribute('disabled');
  }
}
