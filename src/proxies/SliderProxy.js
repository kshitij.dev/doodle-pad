import BaseProxy from './BaseProxy.js';

/**
 * Represents a slider proxy class extending BaseProxy for managing slider elements
 */
export default class SliderProxy extends BaseProxy {
  /**
   * Creates a slider element
   * @param {String} label - The label for the slider
   * @param {Number} min - The minimum value of the slider
   * @param {Number} max - The maximum value of the slider
   * @param {Number} value - The initial value of the slider
   * @param {Number} step - The step increment for the slider
   * @returns {SliderProxy} - The created SliderProxy instance
   */
  create(label, min, max, value, step) {
    this.div = this.p5.createDiv('');
    this.div.parent(this.parent);

    this.element = this.p5.createSlider(min, max, value, step);
    this.element.parent(this.div);
    this.element.style('width', '80px');
    this.span = this.p5.createSpan(`&nbsp ${label}`);
    this.span.parent(this.div);

    return this;
  }

  /**
   * Assigns a function to be executed when the slider value changes
   * @param {Function} func - The function to be executed
   */
  changed(func) {
    if (this.element !== undefined) {
      this.element.changed(func);
    }
  }
}
