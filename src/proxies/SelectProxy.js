import BaseProxy from './BaseProxy.js';

/**
 * Represents a select proxy class extending BaseProxy for managing select elements
 */
export default class SelectProxy extends BaseProxy {
  /**
   * Creates a select element
   * @param {String} label - The label for the select field
   * @param {String[]} list - The list of options for the select field
   * @returns {SelectProxy} - The created SelectProxy instance
   */
  create(label, list) {
    this.div = this.p5.createDiv('');
    this.div.parent(this.parent);

    this.element = this.p5.createSelect();
    this.element.parent(this.div);

    list.forEach((item) => this.element.option(item));

    this.span = this.p5.createSpan(`&nbsp ${label}`);
    this.span.parent(this.div);

    return this;
  }

  /**
   * Assigns a function to be executed when the select field value changes
   * @param {Function} func - The function to be executed
   */
  changed(func) {
    if (this.element !== undefined) {
      this.element.changed(func);
    }
  }

  /**
   * Gets or sets the selected value of the select field
   * @param {String} value - The value to set as selected (optional)
   * @returns {String} - The current or updated selected value
   */
  selected(value) {
    if (value === undefined) {
      return this.element.selected();
    }
    this.element.selected(value);
    return value;
  }
}
