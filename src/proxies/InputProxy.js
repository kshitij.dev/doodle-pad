import BaseProxy from './BaseProxy.js';

/**
 * Represents an input proxy class extending BaseProxy for managing input fields
 */
export default class InputProxy extends BaseProxy {
  /**
   * Creates an input element
   * @param {String} label - The label for the input field
   * @param {String} value - The initial value of the input field
   * @param {p5.Element} div - The parent element for the input field (optional)
   * @returns {InputProxy} - The created InputProxy instance
   */
  create(label, value, div = undefined) {
    if (div === undefined) {
      this.div = this.p5.createDiv('');
      this.div.parent(this.parent);
    } else {
      this.div = div;
    }

    this.element = this.p5.createInput(value);
    this.element.parent(this.div);
    this.element.style('width', '80px');
    this.span = this.p5.createSpan(`&nbsp ${label}`);
    this.span.parent(this.div);

    return this;
  }

  /**
   * Assigns a function to be executed when the input field value changes
   * @param {Function} func - The function to be executed
   */
  changed(func) {
    if (this.element !== undefined) {
      this.element.input(func);
    }
  }
}
