/* eslint-disable class-methods-use-this */
/**
 * Represents a base proxy class providing common functionalities for UI elements
 */
export default class BaseProxy {
  /** @type {p5.Element} - The div element of the proxy */
  div;

  /** @type {p5} - The p5 instance */
  p5;

  /** @type {p5.Element} - The parent element of the proxy */
  parent;

  /** @type {p5.Element} - The main DOM element concerned with this proxy */
  element;

  /** @type {p5.Element} - The span element of the proxy */
  span;

  /**
   * Creates an instance of BaseProxy
   * @param {p5} p5 - The p5.js instance
   * @param {p5.Element} parent - The parent DOM element
   */
  constructor(p5, parent) {
    this.parent = parent;
    this.p5 = p5;
  }

  /**
   * Hides the proxy element
   */
  hide() {
    this.div.hide();
  }

  /**
   * Shows the proxy element
   */
  show() {
    this.div.show();
  }

  /**
   * Gets or sets the value of the proxy element
   * @param {*} val - The value to set (optional)
   * @returns {*} - The current or updated value of the element
   */
  value(val = undefined) {
    if (this.element !== undefined) {
      if (val === undefined) {
        return this.element.value();
      }
      this.element.value(val);
      return val;
    }
    return undefined;
  }

  /**
   * Placeholder for handling selected behaviour
   */
  selected() { }

  /**
   * Placeholder for handling changed behaviour
   */
  changed() { }
}
