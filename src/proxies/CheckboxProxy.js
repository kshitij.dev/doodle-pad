import BaseProxy from './BaseProxy.js';

/**
 * Represents a checkbox proxy class extending BaseProxy for managing checkboxes
 */
export default class CheckboxProxy extends BaseProxy {
  /**
   * Creates a checkbox element
   * @param {String} label - The label for the checkbox
   * @param {Boolean} value - The initial value of the checkbox
   * @returns {CheckboxProxy} - The created CheckboxProxy instance
   */
  create(label, value) {
    this.div = this.p5.createDiv('');
    this.div.parent(this.parent);

    this.element = this.p5.createCheckbox(`&nbsp ${label}`, value);
    this.element.parent(this.div);

    this.span = this.p5.createSpan();
    this.span.parent(this.div);

    return this;
  }

  /**
   * Assigns a function to be executed when the checkbox value changes
   * @param {Function} func - The function to be executed
   */
  changed(func) {
    if (this.element !== undefined) {
      this.element.changed(func);
    }
  }
}
