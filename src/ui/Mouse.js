import InputDevice from './InputDevice.js';

/**
 * A class representing a mouse input device, inheriting from InputDevice
 */
export default class Mouse extends InputDevice {
  /**
   * Update the current position of the mouse
   * Overrides the base class method
   */
  updateCurrentPosition() {
    this.state.currentPosition = this.p5.createVector(this.p5.mouseX, this.p5.mouseY);
  }

  /**
   * Check if the mouse button is pressed
   * Overrides the base class method
   * @returns {Boolean} - True if the mouse button is pressed
   */
  isPressed() {
    this.state.pressed = this.p5.mouseIsPressed;
    return this.state.pressed;
  }
}
