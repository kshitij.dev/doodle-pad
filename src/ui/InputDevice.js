/* eslint-disable class-methods-use-this */

/**
 * Base class responsible for managing an input device, such as a mouse
 */
export default class InputDevice {
  /**
   * The p5 instance
   * @type {p5}
   */
  p5;

  /**
   * The canvas associated with the input device
   * @type {p5.Element}
   */
  canvas;

  /**
   * The state of the input device
   * @type {{
   * pressed: Boolean,
   * isFirstFrame: Boolean,
   * initialPosition: p5.Vector,
   * currentPosition: p5.Vector}}
   */
  state;

  /**
   * Create a new InputDevice instance
   * @param {p5} p5 - The p5 instance
   * @param {p5.Element} canvas - The canvas element
   */
  constructor(p5, canvas) {
    this.p5 = p5;
    this.canvas = canvas;

    this.state = {
      pressed: false,
      isFirstFrame: true,
      initialPosition: p5.createVector(),
      currentPosition: p5.createVector()
    };
  }

  /**
   * Update the current position of the input device
   * (To be overridden by inherited device classes)
   */
  updateCurrentPosition() {
    // Implementation will vary in derived classes
  }

  /**
   * Reset the initial position and initial state of the input device
   */
  resetState() {
    this.state.isFirstFrame = true;
    this.state.pressed = false;
  }

  /**
   * Update the initial position of the input device
   */
  updateInitialPosition() {
    this.state.isFirstFrame = false;
    this.updateCurrentPosition();
    this.state.initialPosition = this.state.currentPosition;
  }

  /**
   * Get the initial position of the input device
   * @returns {p5.Vector} - The initial position
   */
  getInitialPosition() {
    return this.state.initialPosition;
  }

  /**
   * Get the current position of the input device
   * @returns {p5.Vector} - The current position
   */
  getCurrentPosition() {
    return this.state.currentPosition;
  }

  /**
   * Check if the current frame is the first frame of interaction
   * @returns {Boolean} - True if it's the first frame
   */
  isFirstFrame() {
    return this.state.isFirstFrame;
  }

  /**
   * Check if the input device is within the canvas boundaries
   * @returns {Boolean} - True if it's within the canvas
   */
  isInCanvas() {
    this.updateCurrentPosition();
    const { x, y } = this.state.currentPosition;
    return x >= 0 && y >= 0 && x <= this.canvas.size().width && y <= this.canvas.size().height;
  }

  /**
   * Check if the input device is pressed
   * @returns {Boolean} - True if it's pressed
   */
  isPressed() {
    // Implementation will vary in derived classes
  }
}
