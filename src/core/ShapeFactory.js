/**
 * Manages the creation and drawing of various shapes using p5js and Scribble library
 */
export default class ShapeFactory {
  /**
   * The p5 instance
   * @type {p5}
   */
  #p5;

  /**
   * The stroke colour of the shape
   * @type {p5.Color}
   */
  #stroke;

  /**
   * The thickness of the stroke
   * @type {Number}
   */
  #strokeWeight;

  /**
   * The type of shape
   * @type {String}
   */
  #type;

  /**
   * The number of sides in the polygon shape
   * @type {Number}
   */
  #polySides;

  /**
   * The relative angle of the polygon shape
   * @type {Number}
   */
  #polyAngle;

  /**
   * Flag indicating if the stroke is dashed
   * @type {Boolean}
   */
  #isStrokeDashed;

  /**
   * The internal fill colour of the shape
   * @type {p5.Color}
   */
  #fillColour;

  /**
   * Flag indicating if the shape is filled
   * @type {Boolean}
   */
  #isFilled;

  /**
   * Instance of the Scribble class
   * @type {Scribble}
   */
  #scribble;

  /**
   * The graphics buffer or canvas onto which the shape should be drawn
   * @type {p5.Graphics}
   */
  #buffer;

  /**
   * Flag indicating if a vertex shape is closed
   * @type {Boolean}
   */
  #isClosed;

  /**
   * Creates a new instance of a ShapeFactory
   * @param {p5} p5 - The p5 instance
   * @param {Object} config - The properties of the shape
   */
  constructor(p5, config) {
    this.#p5 = p5;

    // Initialise a Scribble instance
    this.#scribble = new Scribble(this.#p5);

    // Validate the config and save to private local fields
    this.#isFilled = 'isFilled' in config ? config.isFilled : false;
    this.#isClosed = 'isClosed' in config ? config.isClosed : false;
    if ('fillColour' in config) this.#fillColour = config.fillColour;
    if ('strokeWeight' in config) this.#strokeWeight = config.strokeWeight;
    if ('stroke' in config) this.#stroke = config.stroke;
    if ('type' in config) this.#type = config.type;
    if ('polySides' in config) this.#polySides = config.polySides;
    if ('polyAngle' in config) this.#polyAngle = config.polyAngle;
    if ('isStrokeDashed' in config && config.isStrokeDashed) {
      this.#isStrokeDashed = config.isStrokeDashed;
    } else {
      this.#isStrokeDashed = false;
    }

    // This class supports both standard canvases and graphics buffers
    this.#buffer = 'buffer' in config ? config.buffer : this.#p5;
  }

  /**
   * Draw the shape
   * @param {p5.Vector} from - The starting point of the shape
   * @param {p5.Vector} to - The end point of the shape
   * @param {p5.Vector[]} otherPoints - Points for complex shape inside beginShape-endShape
   */
  drawShape(from, to, otherPoints = undefined) {
    // Save settings so that only the drawn shape has the settings
    this.#buffer.push();

    this.#configureStroke();
    this.#configureFill();

    // Draw the requested shape
    switch (this.#type) {
      case 'Point':
        this.#buffer.point(from.x, from.y);
        break;

      case 'Line':
        this.#buffer.line(from.x, from.y, to.x, to.y);
        break;

      case 'Ellipse':
        this.#drawEllipse(from, to);
        break;

      case 'Rectangle':
        this.#buffer.rect(from.x, from.y, to.x - from.x, to.y - from.y);
        break;

      case 'ScribbleRectangle':
        this.#drawScribbleRect(to, from);
        break;

      case 'ScribbleEllipse':
        this.#drawScribbleEllipse(to, from);
        break;

      case 'Triangle':
        this.#drawTriangle(to, from);
        break;

      case 'Polygon':
        this.#drawPolygon(to, from);
        break;

      case 'Complex':
        this.#drawComplexShape(otherPoints);
        break;

      default:
        // Draw nothing
        break;
    }

    // Restore the settings
    this.#buffer.pop();
  }

  /**
   * Draws edges between the points and closes the shape if requested
   * @param {p5.Vector[]} points - The vertices of the edges
   */
  #drawComplexShape(points) {
    // Only draw the edges if points is defined and an array
    if (points !== undefined && Array.isArray(points)) {
      this.#buffer.beginShape();

      // Loop through each point and define the vertex
      points.forEach((point) => {
        this.#buffer.vertex(point.x, point.y);
      });

      // Close the shape if requested
      if (this.#isClosed) {
        this.#buffer.endShape(this.#p5.CLOSE);
      } else {
        this.#buffer.endShape();
      }
    }
  }

  /**
   * Draw an n-sided polygon with a user-defined rotation relative to default
   * @param {p5.Vector} from - Origin of the imaginary rectangle within which the polygon is drawn
   * @param {p5.Vector} to - Opposite corner
   */
  #drawPolygon(to, from) {
    // Constrain the sides to 20 as any higher would be better off drawn as a circle/ellipse
    const sideCount = this.#p5.constrain(parseInt(this.#polySides.toString(), 10), 3, 20);

    // Convert the angle to radians, as working with SI units everywhere
    const angleRadians = (Math.PI * parseInt(this.#polyAngle.toString(), 10)) / 180;

    // Work out the required constants
    const w = to.x - from.x;
    const h = to.y - from.y;
    const r = Math.sqrt(w * w + h * h);

    // Draw the polygon as a closed shape, working out the coordinates of the vertices at each step
    this.#buffer.beginShape();
    for (let i = 0; i < sideCount; i++) {
      this.#buffer.vertex(
        from.x + Math.cos(angleRadians + (i * Math.PI * 2) / sideCount) * r,
        from.y + Math.sin(angleRadians + (i * Math.PI * 2) / sideCount) * r
      );
    }
    this.#buffer.endShape(this.#p5.CLOSE);
  }

  /**
   * Draw a triangle
   * @param {p5.Vector} from - Origin of one of the vertices of the triangle
   * @param {p5.Vector} to - Position of the mid-point of the side opposite the vertex
   */
  #drawTriangle(to, from) {
    // Calculate the height of the triangle and only draw if it is positive definite
    const h = to.dist(from);
    if (h <= 0) return;

    // Calculate the coordinates of the other two vertices of the triangle
    const v2 = this.#p5.createVector(from.x - h, from.y - h);
    const v3 = this.#p5.createVector(from.x + h, from.y - h);

    // Define the unit vectors and calculate the angle between them
    const uv = this.#p5.createVector((to.x - from.x) / h, (to.y - from.y) / h);
    const ux = this.#p5.createVector(0, -1);
    const angle = ux.angleBetween(uv);

    // Rotate the coordinates of the other two vertices
    const v2R = this.#p5.createVector(
      from.x + (v2.x - from.x) * Math.cos(angle) - (v2.y - from.y) * Math.sin(angle),
      from.y + (v2.x - from.x) * Math.sin(angle) + (v2.y - from.y) * Math.cos(angle)
    );

    const v3R = this.#p5.createVector(
      from.x + (v3.x - from.x) * Math.cos(angle) - (v3.y - from.y) * Math.sin(angle),
      from.y + (v3.x - from.x) * Math.sin(angle) + (v3.y - from.y) * Math.cos(angle)
    );

    // Draw the rotated triangle
    this.#buffer.triangle(from.x, from.y, v2R.x, v2R.y, v3R.x, v3R.y);
  }

  /**
   * Draws a "rough" ellipse
   * @param {p5.Vector} from - Origin of the imaginary rectangle within which the ellipse is drawn
   * @param {p5.Vector} to - Opposite corner
   */
  #drawScribbleEllipse(to, from) {
    // Use Perlin noise to control the randomness. This way, there is less flicker when drawing
    this.#p5.randomSeed(this.#p5.noise(from.x + from.y) * 100);

    // Calculate the inputs required for Scribble.scribbleEllipse
    const w = to.x - from.x;
    const h = to.y - from.y;
    this.#scribble.scribbleEllipse(from.x + w / 2, from.y + h / 2, w, h);
  }

  /**
   * Draws a "rough" rectangle
   * @param {p5.Vector} to - Opposite corner of the rectangle
   * @param {p5.Vector} from - Origin and one corner of the rectangle
   */
  #drawScribbleRect(to, from) {
    // Use Perlin noise - same reason as for the ellipse
    this.#p5.randomSeed(this.#p5.noise(from.x + from.y) * 100);

    // Calculate the inputs required for Scribble.scribbleEllipse (coordinates centered mode)
    const w = to.x - from.x;
    const h = to.y - from.y;
    const x = from.x + w / 2;
    const y = from.y + h / 2;
    this.#scribble.scribbleRect(x, y, w, h);

    // Optionally, if the user has activated fill, include rough fill instead of standard fill
    if (this.#isFilled && this.#fillColour !== undefined) {
      this.fillHachure(x, y, w, h);
    }
  }

  /**
   * Draws an "exact" ellipse
   * @param {p5.Vector} from - Origin of the ellipse
   * @param {p5.Vector} to - Opposite corner of the imaginary bounding box of the ellipse
   */
  #drawEllipse(from, to) {
    const w = 2 * (from.x - to.x);
    const h = 2 * (from.y - to.y);
    this.#buffer.ellipse(from.x, from.y, w, h);
  }

  /**
   * Validate and apply the fill settings
   */
  #configureFill() {
    if (this.#isFilled && this.#fillColour !== undefined) {
      this.#buffer.fill(this.#fillColour);
    } else {
      this.#buffer.noFill();
    }
  }

  /**
   * Validate and apply the stroke settings
   */
  #configureStroke() {
    if (this.#stroke !== undefined) {
      this.#buffer.stroke(this.#stroke);
    }

    if (this.#strokeWeight !== undefined) {
      if (this.#strokeWeight === 0) {
        this.#buffer.noStroke();
      } else {
        this.#buffer.strokeWeight(this.#strokeWeight);
      }
    }

    if (this.#isStrokeDashed) {
      this.#buffer.drawingContext.setLineDash([5, 10, 30, 10]);
    }
  }

  /**
   * Fills the rectangle with hachure
   * Note: This method is adapted from p5.scribble.js example
   * @param {Number} x - Left coordinate of the rectangle
   * @param {Number} y - Top coordinate of the rectangle
   * @param {Number} w - Width of the rectangle
   * @param {Number} h - Height of the rectangle
   */
  fillHachure(x, y, w, h) {
    // Calculate the x and y coordinates for the border points of the hachure
    const w2 = Math.abs(w / 2);
    const h2 = Math.abs(h / 2);

    const xLeft = x - w2 + this.#strokeWeight;
    const xRight = x + w2 - this.#strokeWeight;
    const yTop = y - h2 + this.#strokeWeight;
    const yBottom = y + h2 - this.#strokeWeight;

    // Copied code starts here ------>

    // The x coordinates of the border points of the hachure
    const xCoords = [xLeft, xRight, xRight, xLeft];

    // The y coordinates of the border points of the hachure
    const yCoords = [yTop, yTop, yBottom, yBottom];

    // The gap between two hachure lines
    const gap = 3.5;

    // The angle of the hachure in degrees
    const angle = 315;

    this.#buffer.push();
    // Set the thickness of hachure lines
    this.#buffer.strokeWeight(3);

    // Set the colour of the hachure
    this.#buffer.stroke(this.#fillColour);

    // Fill the rect with a hachure
    this.#scribble.scribbleFilling(xCoords, yCoords, gap, angle);

    this.#buffer.pop();

    // <------- Copied code ends here
  }
}
