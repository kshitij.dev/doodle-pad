import HistoryManager from '../managers/HistoryManager.js';
import SaveFileManager from '../managers/SaveFileManager.js';
import ColourPalette from '../managers/ColourPalette.js';
import Toolbox from '../managers/Toolbox.js';
import StrokeWeightManager from '../managers/StrokeWeightManager.js';
import GraphicsBufferManager from '../managers/GraphicsBufferManager.js';

/**
 * This function, inspired from the "Mediator" design pattern is responsible to
 * collate the various parts of the app and enable communication between them through the
 * p5 instance
 * @param p5 {p5}
 * @constructor
 */
export default function SketchMediator(p5) {
  let instance = {};

  /**
   * The setup function for this p5 instance
   */
  p5.setup = () => {
    // get the canvas container div
    const canvasContainer = p5.select('#content');

    // Turn off pixel density matching display density. This is a workaround
    // for a limitation affecting flood fill algorithm, where pixel coordinates
    // are not always correctly found on displays with very high display density
    p5.pixelDensity(1);

    // initialise the instance object and add the various managers, each of which is
    // responsible for a part of the app
    instance = {
      p5,
      canvasContainer,
      canvas: p5.createCanvas(canvasContainer.size().width, canvasContainer.size().height),
      helpers: new SaveFileManager(p5),
      historyManager: new HistoryManager(p5),
      colourPalette: new ColourPalette(p5),
      toolbox: new Toolbox(p5),
      strokeWeightManager: new StrokeWeightManager(p5),
      graphicsBufferManager: new GraphicsBufferManager(p5)
    };

    // make the canvas instance a child of the content div
    instance.canvas.parent('content');

    // add the instance to the colour palette
    instance.colourPalette.setInstance(instance);

    // initialise the toolbox
    instance.toolbox.setup(instance);
  };

  /**
   * Resize the canvas and any related graphics buffers when the user resizes the browser window
   */
  p5.windowResized = () => {
    p5.resizeCanvas(instance.canvasContainer.size().width, instance.canvasContainer.size().height);
    instance.graphicsBufferManager.resizeAll(
      instance.canvasContainer.size().width,
      instance.canvasContainer.size().height
    );
    instance.historyManager.redraw();
  };

  // Cascade the draw call to current instance's toolbox
  p5.draw = () => {
    instance.toolbox.draw();
  };

  // Cascade the mouseDragged call to current instance's toolbox
  p5.mouseDragged = () => {
    instance.toolbox.mouseDragged();
  };

  // Cascade the mousePressed call to current instance's toolbox
  p5.mousePressed = () => {
    instance.toolbox.mousePressed();
  };

  // Cascade the mouseReleased call to current instance's toolbox
  p5.mouseReleased = () => {
    instance.toolbox.mouseReleased();
  };
}
