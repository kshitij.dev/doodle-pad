# Doodle Pad 🖌️

Welcome to **Doodle Pad**, a dynamic drawing app built using vanilla JavaScript and p5.js, complemented with minimal HTML and CSS.

The demo can be found [here](https://doodle-pad-kmg14.vercel.app/).

## Features ✨

### Pattern Tool 🎨
- **Customizable Patterns**: Draw with patterns instead of solid colors.
- **Graphics Buffer Manager**: Uses the observer design pattern to manage pattern graphics buffers.
- **Live Preview**: Each pattern can be previewed live, with real-time updates when settings change.
- **Responsive**: Adapts to browser window resizes.

### Undo-Redo ↩️↪️
- **History Manager**: Efficiently manages stacks of p5 pixel arrays.
- **Compatibility**: Works seamlessly with all tools.
- **Intuitive Handling**: Manages mirrored objects, sketch clearing, and ignores drawing aids.

### Bucket Fill 🪣
- **Color Picker**: Select fill color and adjust opacity.
- **Efficient Algorithm**: Utilizes an optimized, non-recursive flood fill algorithm.

### Geometric Shapes 🔺🔵
- **Customizable Shapes**: Create various standard shapes and customizable polygons.
- **Rotatable Triangles**: Rotate triangles with the mouse, pivoting at one vertex.

### Other Extensions & Features 🌟
- **Editable Shape Tool**: Uses async timeout callbacks for point editing.
- **Undoable Scissor and Eraser Tools**: Allows undoing actions for precise edits.
- **Adjustable Stroke Weight**: Customize stroke weight for relevant tools.
- **Modern JavaScript**: Implements ES6 features, OOP principles, and advanced coding techniques.
- **AirBnB JavaScript Style Guide Compliance**: Ensures clean and readable code.
- **Advanced File Loading**: Uses import/export with p5.js in instance mode for clean namespace management.
- **Well-organized Codebase**: Adheres to SOLID and DRY principles for maintainability.

## Design Patterns Used 🛠️
- **Proxy**: For DOM interactions.
- **Factory**: For creating shapes and tools.
- **State**: For managing input devices.
- **Mediator**: Manages interaction across tools and features.

## Getting Started 🚀

### Prerequisites
- Ensure you have a modern web browser installed (Chrome, Firefox, Edge, etc.).


### Usage
- Open the [deployed app](https://doodle-pad-kmg14.vercel.app/) in your web browser to start using Doodle Pad.

## Contributing 🤝

1. **Fork the repository**.
2. **Create a new branch**:
    ```bash
    git checkout -b feature-branch
    ```
3. **Commit your changes**:
    ```bash
    git commit -m 'Add new feature'
    ```
4. **Push to the branch**:
    ```bash
    git push origin feature-branch
    ```
5. **Open a pull request**.

## License 📄

This project is licensed under the GPL3 License - see the [LICENSE](LICENSE) file for details.

## Acknowledgements 🙌

- **p5.js**: A JavaScript library that makes coding accessible for artists, designers, educators, and beginners.
- **AirBnB JavaScript Style Guide**: For providing coding style guidelines.

## Contact 📧

For any inquiries or issues, please create an issue.

Happy Doodling! 🎉